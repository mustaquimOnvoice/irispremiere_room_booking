-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2020 at 08:52 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbhotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bsi_admin`
--

CREATE TABLE `bsi_admin` (
  `id` int(11) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT 'admin',
  `access_id` int(1) NOT NULL DEFAULT '0',
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bsi_admin`
--

INSERT INTO `bsi_admin` (`id`, `pass`, `username`, `access_id`, `f_name`, `l_name`, `email`, `designation`, `last_login`, `status`) VALUES
(1, '21232f297a57a5a743894a0e4a801fc3', 'admin', 1, 'admin', 'admin', 'admin@example.com', 'Administrator', '2020-01-28 07:05:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_adminmenu`
--

CREATE TABLE `bsi_adminmenu` (
  `id` int(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `parent_id` int(4) DEFAULT '0',
  `status` enum('Y','N') CHARACTER SET latin1 DEFAULT 'Y',
  `ord` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bsi_adminmenu`
--

INSERT INTO `bsi_adminmenu` (`id`, `name`, `url`, `parent_id`, `status`, `ord`) VALUES
(6, 'SETTING', '#', 0, 'Y', 9),
(31, 'Global Setting', 'global_setting.php', 6, 'Y', 1),
(33, 'HOTEL MANAGER', '#', 0, 'Y', 2),
(34, 'Room Manager', 'room_list.php', 33, 'Y', 1),
(35, 'RoomType Manager', 'roomtype.php', 33, 'Y', 2),
(36, 'PricePlan Manager', 'priceplan.php', 63, 'Y', 4),
(37, 'BOOKING MANAGER', '#', 0, 'Y', 4),
(39, 'View Booking List', 'view_bookings.php', 37, 'Y', 2),
(43, 'Payment Gateway', 'payment_gateway.php', 6, 'N', 4),
(44, 'Email Contents', 'email_content.php', 6, 'Y', 5),
(59, 'Capacity Manager', 'admin_capacity.php', 33, 'Y', 3),
(61, 'Advance Payment', 'advance_payment.php', 63, 'N', 6),
(63, 'PRICE MANAGER', '#', 0, 'Y', 3),
(66, 'Hotel Details', 'admin_hotel_details.php', 33, 'Y', 0),
(68, 'Room Blocking', 'admin_block_room.php', 37, 'N', 6),
(70, 'Calendar View', 'calendar_view.php', 37, 'Y', 5),
(71, 'Customer Lookup', 'customerlookup.php', 37, 'Y', 4),
(72, 'Admin Menu Manager', 'adminmenu.list.php', 6, 'N', 6),
(73, 'LANGUAGE MANAGER', '#', 0, 'N', 6),
(74, 'Manage Languages', 'manage_langauge.php', 73, 'N', 1),
(75, 'Special Offer', 'view_special_offer.php', 63, 'Y', 5),
(76, 'CURRENCY MANAGER', '#', 0, 'N', 7),
(77, 'Manage Currency', 'currency_list.php', 76, 'N', 1),
(78, 'Photo Gallery', 'gallery_list.php', 33, 'Y', 7);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_advance_payment`
--

CREATE TABLE `bsi_advance_payment` (
  `month_num` int(11) NOT NULL,
  `month` varchar(255) NOT NULL,
  `deposit_percent` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_advance_payment`
--

INSERT INTO `bsi_advance_payment` (`month_num`, `month`, `deposit_percent`) VALUES
(1, 'January', '0.00'),
(2, 'February', '0.00'),
(3, 'March', '0.00'),
(4, 'April', '0.00'),
(5, 'May', '0.00'),
(6, 'June', '0.00'),
(7, 'July', '0.00'),
(8, 'August', '0.00'),
(9, 'September', '0.00'),
(10, 'October', '0.00'),
(11, 'November', '0.00'),
(12, 'December', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_bookings`
--

CREATE TABLE `bsi_bookings` (
  `booking_id` int(10) UNSIGNED NOT NULL,
  `booking_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  `child_count` int(2) NOT NULL DEFAULT '0',
  `extra_guest_count` int(2) NOT NULL DEFAULT '0',
  `discount_coupon` varchar(50) DEFAULT NULL,
  `total_cost` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `payment_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_type` varchar(255) NOT NULL,
  `payment_success` tinyint(1) NOT NULL DEFAULT '0',
  `payment_txnid` varchar(100) DEFAULT NULL,
  `paypal_email` varchar(500) DEFAULT NULL,
  `special_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `special_requests` text,
  `is_block` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `block_name` varchar(255) DEFAULT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_bookings`
--

INSERT INTO `bsi_bookings` (`booking_id`, `booking_time`, `start_date`, `end_date`, `client_id`, `child_count`, `extra_guest_count`, `discount_coupon`, `total_cost`, `payment_amount`, `payment_type`, `payment_success`, `payment_txnid`, `paypal_email`, `special_id`, `special_requests`, `is_block`, `is_deleted`, `block_name`, `updated_on`) VALUES
(1573298044, '2019-11-09 16:44:04', '2019-11-09', '2019-11-10', 1, 0, 0, NULL, '900.00', '900.00', 'st', 1, NULL, NULL, 0, '', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573301835, '2019-11-09 17:47:15', '2019-11-10', '2019-11-12', 2, 0, 0, NULL, '1520.00', '1520.00', 'poa', 1, NULL, NULL, 0, 'this is demo ', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573302239, '2019-11-09 17:53:59', '2019-11-09', '2019-11-10', 1, 0, 0, NULL, '450.00', '450.00', 'poa', 1, NULL, NULL, 0, 'sad', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573302725, '2019-11-09 18:02:05', '2019-11-09', '2019-11-10', 1, 0, 0, NULL, '1350.00', '1350.00', 'poa', 1, NULL, NULL, 0, 'sasd', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573302822, '2019-11-09 18:03:42', '2019-11-09', '2019-11-11', 1, 0, 0, NULL, '2700.00', '2700.00', 'poa', 1, NULL, NULL, 0, 'sdf  asd', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573304449, '2019-11-09 18:30:49', '2019-11-11', '2019-11-13', 1, 0, 0, NULL, '1000.00', '1000.00', 'poa', 1, NULL, NULL, 0, 'sdf 4 wweweq', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573465633, '2019-11-11 15:17:13', '2019-11-11', '2019-11-12', 3, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, 'dg2423refw3 3rwe', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573469136, '2019-11-11 16:15:36', '2019-11-11', '2019-11-12', 4, 0, 0, NULL, '3000.00', '3000.00', 'poa', 1, NULL, NULL, 0, 'sdfff fff', 0, 1, NULL, '2020-01-28 13:49:33'),
(1573469167, '2019-11-11 16:16:07', '2019-11-11', '2019-11-12', 5, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, 'asd', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573469186, '2019-11-11 16:16:26', '2019-11-11', '2019-11-12', 6, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, ' f sdf ', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573469528, '2019-11-11 16:22:08', '2019-11-12', '2019-11-13', 7, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, 'yyrty', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573469552, '2019-11-11 16:22:32', '2019-11-13', '2019-11-15', 8, 0, 0, NULL, '1000.00', '1000.00', 'poa', 1, NULL, NULL, 0, ' btrd g', 0, 0, NULL, '2020-01-28 13:49:33'),
(1573534814, '2019-11-12 10:30:14', '2019-11-12', '2019-11-13', 9, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, 'sf df sdf sdf sdf', 0, 0, NULL, '2020-01-28 13:49:33'),
(1578900299, '2020-01-13 12:54:59', '2020-01-13', '2020-01-14', 10, 0, 0, NULL, '1500.00', '1500.00', 'poa', 1, NULL, NULL, 0, '', 0, 0, NULL, '2020-01-28 13:49:33'),
(1579754452, '2020-01-23 10:10:52', '2020-01-23', '2020-01-24', 11, 0, 0, NULL, '800.00', '800.00', 'poa', 1, NULL, NULL, 0, '', 0, 0, NULL, '2020-01-28 13:49:33'),
(1580194617, '2020-01-28 12:26:57', '2020-01-28', '2020-01-29', 13, 0, 0, NULL, '900.00', '900.00', 'pum', 1, NULL, NULL, 0, '', 0, 0, NULL, '2020-01-28 13:49:33'),
(1582285856, '2020-02-21 17:20:57', '2020-02-21', '2020-02-22', 14, 0, 0, NULL, '800.00', '800.00', 'poa', 1, NULL, NULL, 0, '', 0, 0, NULL, '2020-02-21 17:20:57');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_capacity`
--

CREATE TABLE `bsi_capacity` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_capacity`
--

INSERT INTO `bsi_capacity` (`id`, `title`, `capacity`) VALUES
(1, 'Single', 1),
(2, 'Double', 2),
(3, 'Triple', 3);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_cc_info`
--

CREATE TABLE `bsi_cc_info` (
  `booking_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `card_type` varchar(50) NOT NULL,
  `card_number` blob NOT NULL,
  `expiry_date` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ccv2_no` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bsi_clients`
--

CREATE TABLE `bsi_clients` (
  `client_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `title` varchar(16) DEFAULT NULL,
  `street_addr` text,
  `city` varchar(64) DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  `zip` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `fax` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `id_type` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `additional_comments` text,
  `ip` varchar(32) DEFAULT NULL,
  `existing_client` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_clients`
--

INSERT INTO `bsi_clients` (`client_id`, `first_name`, `surname`, `title`, `street_addr`, `city`, `province`, `zip`, `country`, `phone`, `fax`, `email`, `password`, `id_type`, `id_number`, `additional_comments`, `ip`, `existing_client`) VALUES
(1, 'Smita', 'iranai', 'Mr', 'mangi sadasd asdsad asdasas', 'Ahmednagar', 'Maharashtra', '414001', 'India', '9175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'sds213213', 'sdf 4 wweweq', '::1', 1),
(2, 'Musa', 'Patel', 'Mr.', 'mukundnagar', 'Ahmednagar', 'Maharashtra', '414001', 'India', '8087586743', '', 'msayyed.it@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ID', '56565/-9898', 'this is demo ', '::1', 1),
(3, 'Doremo', 'mon', 'Mr.', 'example roaad', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'ssfd21421', 'dg2423refw3 3rwe', '::1', 1),
(4, 'da', 'fsa', 'Dr.', 'eraff', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'ase213asd', 'sdfff fff', '::1', 1),
(5, 'Frim', 'fs', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', '324f234', 'asd', '::1', 1),
(6, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', '324fdsf ', ' f sdf ', '::1', 1),
(7, 'gsd', ' as', 'Mr.', 'asd sc asc', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'sad234', 'yyrty', '::1', 1),
(8, 'rty ', 'rtyer', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'er54345', ' btrd g', '::1', 1),
(9, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'sdf35', 'sf df sdf sdf sdf', '::1', 1),
(10, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', '', 'er54345', '', '::1', 1),
(11, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', '', 'er54345', '', '::1', 1),
(12, 'Mustaquim', 'Sayyed', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '9175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', '', 'testidnum', '', '::1', 1),
(13, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '09175917762', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'er54345', '', '::1', 1),
(14, 'Musa', 'Sa', 'Mr.', 'anuron', 'Ahmednagar', 'Maharashtra', '414001', 'India', '8087586743', '', 'mustaquim.sayyed@onevoice.co.in', 'e10adc3949ba59abbe56e057f20f883e', 'ID', 'er54345', '', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_configure`
--

CREATE TABLE `bsi_configure` (
  `conf_id` int(11) NOT NULL,
  `conf_key` varchar(100) NOT NULL,
  `conf_value` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='bsi hotel configurations';

--
-- Dumping data for table `bsi_configure`
--

INSERT INTO `bsi_configure` (`conf_id`, `conf_key`, `conf_value`) VALUES
(1, 'conf_hotel_name', 'Iris Premiere'),
(2, 'conf_hotel_streetaddr', 'Opp Anand Rishiji Hospital, Station Road'),
(3, 'conf_hotel_city', 'Ahmednagar'),
(4, 'conf_hotel_state', 'Maharashtra'),
(5, 'conf_hotel_country', 'India'),
(6, 'conf_hotel_zipcode', '414001'),
(7, 'conf_hotel_phone', '+91 90111 37000'),
(8, 'conf_hotel_fax', '+91 241 2327000'),
(9, 'conf_hotel_email', 'mustaquim.sayyed@onevoice.co.in'),
(20, 'conf_tax_amount', ''),
(21, 'conf_dateformat', 'dd/mm/yy'),
(22, 'conf_booking_exptime', '1000'),
(25, 'conf_enabled_deposit', '0'),
(26, 'conf_hotel_timezone', 'Asia/Calcutta'),
(27, 'conf_booking_turn_off', '0'),
(28, 'conf_min_night_booking', '1'),
(30, 'conf_notification_email', 'mustaquim.sayyed@onevoice.co.in'),
(31, 'conf_price_with_tax', '0'),
(32, 'conf_maximum_global_years', '365'),
(33, 'conf_payment_currency', '0'),
(34, 'conf_invoice_currency', '0'),
(35, 'conf_currency_update_time', '1582519027');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_currency`
--

CREATE TABLE `bsi_currency` (
  `id` int(11) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `currency_symbl` varchar(10) NOT NULL,
  `exchange_rate` decimal(20,6) NOT NULL,
  `default_c` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_currency`
--

INSERT INTO `bsi_currency` (`id`, `currency_code`, `currency_symbl`, `exchange_rate`, `default_c`) VALUES
(1, 'INR', 'Rs', '1.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_email_contents`
--

CREATE TABLE `bsi_email_contents` (
  `id` int(11) NOT NULL,
  `email_name` varchar(500) NOT NULL,
  `email_subject` varchar(500) NOT NULL,
  `email_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_email_contents`
--

INSERT INTO `bsi_email_contents` (`id`, `email_name`, `email_subject`, `email_text`) VALUES
(1, 'Confirmation Email', 'Confirmation of your successfull booking in our hotel', '<p><strong>Text can be chnage in admin panel</strong></p>\r\n'),
(2, 'Cancellation Email ', 'Cancellation Email subject', '<p><strong>Text can be chnage in admin panel</strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_gallery`
--

CREATE TABLE `bsi_gallery` (
  `pic_id` int(11) NOT NULL,
  `roomtype_id` int(11) NOT NULL,
  `capacity_id` int(11) NOT NULL,
  `img_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_gallery`
--

INSERT INTO `bsi_gallery` (`pic_id`, `roomtype_id`, `capacity_id`, `img_path`) VALUES
(1, 1, 1, '1573297832_PS-img-3.jpg'),
(2, 1, 1, '1573297908_PE-img-2.jpg'),
(3, 2, 2, '1573297908_PE-img-2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_invoice`
--

CREATE TABLE `bsi_invoice` (
  `booking_id` int(10) NOT NULL,
  `client_name` varchar(500) NOT NULL,
  `client_email` varchar(500) NOT NULL,
  `invoice` longtext NOT NULL,
  `booking_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_invoice`
--

INSERT INTO `bsi_invoice` (`booking_id`, `client_name`, `client_email`, `invoice`, `booking_time`) VALUES
(1573298044, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573298044</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">09/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">10/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">2</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs900.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs900.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs900.00</td></tr></tbody></table>', '2020-01-28 13:41:35'),
(1573301835, 'Musa Patel', 'msayyed.it@gmail.com', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573301835</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Patel</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">10/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs1,520.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,520.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,520.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">this is demo </td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573302239, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573302239</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">09/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">10/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs450.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs450.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs450.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sad</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573302725, 'Harun Bhai', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573302725</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Harun Bhai</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">09/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">10/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,350.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,350.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,350.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sasd</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573302822, 'Esa Masi', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573302822</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Esa Masi</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">09/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs2,700.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs2,700.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs2,700.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sdf  asd</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573304449, 'Smita iranai', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573304449</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Smita iranai</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">13/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,000.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,000.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,000.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sdf 4 wweweq</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573465633, 'Doremo mon', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573465633</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Doremo mon</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">dg2423refw3 3rwe</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573469136, 'da fsa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573469136</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">da fsa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">2</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs3,000.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs3,000.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs3,000.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sdfff fff</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573469167, 'Frim fs', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573469167</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Frim fs</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">asd</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573469186, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573469186</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">11/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\"> f sdf </td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573469528, 'gsd  as', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573469528</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">gsd  as</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">13/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">yyrty</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1573469552, 'rty  rtyer', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573469552</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">rty  rtyer</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">13/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">15/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,000.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,000.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,000.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\"> btrd g</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35');
INSERT INTO `bsi_invoice` (`booking_id`, `client_name`, `client_email`, `invoice`, `booking_time`) VALUES
(1573534814, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1573534814</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">12/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">13/11/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">sf df sdf sdf sdf</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1578900299, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1578900299</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">13/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">14/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Superior (Double)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs1,500.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs1,500.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1579754452, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1579754452</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">23/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">24/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs800.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs800.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs800.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-01-28 13:41:35'),
(1580194617, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1580194617</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">28/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">29/01/2020</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">2</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">2</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult </td><td align=\"right\" style=\"background:#ffffff;\">Rs900.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs900.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs900.00</td></tr></tbody></table>', '2020-01-28 13:41:35'),
(1582285856, 'Musa Sa', 'mustaquim.sayyed@onevoice.co.in', '<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1582285856</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Musa Sa</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">21/02/2020</td><td align=\"center\" style=\"background:#ffffff;\">22/02/2020</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Premiere Executive (Single)</td><td align=\"center\" style=\"background:#ffffff;\">1 Adult  + 1 Child</td><td align=\"right\" style=\"background:#ffffff;\">Rs800.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs800.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Rs800.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>', '2020-02-21 17:20:57');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_language`
--

CREATE TABLE `bsi_language` (
  `id` int(11) NOT NULL,
  `lang_title` varchar(255) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `lang_file` varchar(255) NOT NULL,
  `lang_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_language`
--

INSERT INTO `bsi_language` (`id`, `lang_title`, `lang_code`, `lang_file`, `lang_default`) VALUES
(1, 'English', 'en', 'english.php', 1),
(2, 'French', 'fr', 'french.php', 0),
(3, 'German', 'de', 'german.php', 0),
(4, 'Greek', 'el', 'greek.php', 0),
(5, 'Spanish', 'es', 'espanol.php', 0),
(6, 'Italian', 'it', 'italian.php', 0),
(7, 'Dutch', 'nl', 'dutch.php', 0),
(8, 'Polish', 'pl', 'polish.php', 0),
(9, 'Portuguese', 'pt', 'portuguese.php', 0),
(10, 'Russian', 'ru', 'russian.php', 0),
(11, 'Turkish', 'tr', 'turkish.php', 0),
(12, 'Thai', 'th', 'thai.php', 0),
(13, 'Chinese', 'zh-CN', 'chinese.php', 0),
(14, 'Indonesian', 'id', 'indonesian.php', 0),
(15, 'Romanian', 'ro', 'romanian.php', 0),
(17, 'Japanese', 'ja', 'japanese.php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_payment_gateway`
--

CREATE TABLE `bsi_payment_gateway` (
  `id` int(11) NOT NULL,
  `gateway_name` varchar(255) NOT NULL,
  `gateway_code` varchar(50) NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_payment_gateway`
--

INSERT INTO `bsi_payment_gateway` (`id`, `gateway_name`, `gateway_code`, `account`, `enabled`) VALUES
(1, 'PayPal', 'pp', 'phpdev_1330251667_biz@aol.com', 0),
(2, 'Manual', 'poa', 'null', 1),
(3, 'Credit Card (offline)', 'cc', 'null', 0),
(4, 'Authorize.Net', 'an', '8nZ8py3MY=|=6936uG8m8hKFRP6A=|=bestsoftinc', 0),
(5, '2Checkout', '2co', '2002812', 0),
(6, 'Stripe', 'st', 'sk_test_HTGxkUdVdAGd3t0dFqFomE40#pk_test_c6KEy6DebkozRP0V9ysSDwVq#1', 0),
(7, 'PayUmoney', 'pum', 'rTF4fsty#8eEwb2ww1v', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_priceplan`
--

CREATE TABLE `bsi_priceplan` (
  `plan_id` int(10) NOT NULL,
  `roomtype_id` int(10) DEFAULT NULL,
  `capacity_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sun` decimal(10,2) DEFAULT '0.00',
  `mon` decimal(10,2) DEFAULT '0.00',
  `tue` decimal(10,2) DEFAULT '0.00',
  `wed` decimal(10,2) DEFAULT '0.00',
  `thu` decimal(10,2) DEFAULT '0.00',
  `fri` decimal(10,2) DEFAULT '0.00',
  `sat` decimal(10,2) DEFAULT '0.00',
  `default_plan` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_priceplan`
--

INSERT INTO `bsi_priceplan` (`plan_id`, `roomtype_id`, `capacity_id`, `start_date`, `end_date`, `sun`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `default_plan`) VALUES
(1, 1, 1, '0000-00-00', '0000-00-00', '500.00', '500.00', '500.00', '500.00', '500.00', '500.00', '500.00', 1),
(2, 1, 2, '0000-00-00', '0000-00-00', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', 1),
(3, 1, 3, '0000-00-00', '0000-00-00', '1300.00', '1300.00', '1300.00', '1300.00', '1300.00', '1300.00', '1300.00', 1),
(4, 1, 1001, '0000-00-00', '0000-00-00', '300.00', '300.00', '300.00', '300.00', '300.00', '300.00', '300.00', 1),
(5, 2, 1, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(6, 2, 2, '0000-00-00', '0000-00-00', '1500.00', '1500.00', '1500.00', '1500.00', '1500.00', '1500.00', '1500.00', 1),
(7, 2, 3, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(8, 2, 1001, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(9, 3, 1, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(10, 3, 2, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(11, 3, 3, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1),
(12, 3, 1001, '0000-00-00', '0000-00-00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bsi_reservation`
--

CREATE TABLE `bsi_reservation` (
  `id` int(11) NOT NULL,
  `bookings_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_reservation`
--

INSERT INTO `bsi_reservation` (`id`, `bookings_id`, `room_id`, `room_type_id`, `last_update`) VALUES
(1, 1573298044, 1, 1, '2020-02-21 17:29:47'),
(2, 1573298044, 2, 1, '2020-02-21 17:29:47'),
(4, 1573301835, 1, 1, '2020-02-21 17:29:47'),
(5, 1573302239, 10, 1, '2020-02-21 17:29:47'),
(6, 1573302725, 11, 2, '2020-02-21 17:29:47'),
(7, 1573302822, 12, 2, '2020-02-21 17:29:47'),
(8, 1573304449, 10, 1, '2020-02-21 17:29:47'),
(9, 1573465633, 11, 2, '2020-02-21 17:29:47'),
(10, 1573469136, 12, 2, '2020-02-21 17:29:47'),
(11, 1573469136, 13, 2, '2020-02-21 17:29:47'),
(12, 1573469167, 14, 2, '2020-02-21 17:29:47'),
(13, 1573469186, 15, 2, '2020-02-21 17:29:47'),
(15, 1573469528, 11, 2, '2020-02-21 17:29:47'),
(16, 1573469552, 10, 1, '2020-02-21 17:29:47'),
(17, 1573534814, 12, 2, '2020-02-21 17:29:47'),
(18, 1578900299, 11, 2, '2020-02-21 17:29:47'),
(19, 1579754452, 10, 1, '2020-02-21 17:29:47'),
(20, 1580194617, 10, 1, '2020-02-21 17:29:47'),
(21, 1580194617, 16, 1, '2020-02-21 17:29:47'),
(22, 1582285856, 10, 1, '2020-02-21 17:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_room`
--

CREATE TABLE `bsi_room` (
  `room_ID` int(10) NOT NULL,
  `roomtype_id` int(10) DEFAULT NULL,
  `room_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `capacity_id` int(10) DEFAULT NULL,
  `no_of_child` int(11) NOT NULL DEFAULT '0',
  `extra_bed` tinyint(1) DEFAULT '0',
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_room`
--

INSERT INTO `bsi_room` (`room_ID`, `roomtype_id`, `room_no`, `capacity_id`, `no_of_child`, `extra_bed`, `last_update`) VALUES
(10, 1, '10', 1, 2, 0, '2020-02-21 17:31:29'),
(11, 2, '11', 2, 1, 0, '2020-02-21 17:31:29'),
(12, 2, '12', 2, 1, 0, '2020-02-21 17:31:29'),
(13, 2, '13', 2, 1, 0, '2020-02-21 17:31:29'),
(14, 2, '14', 2, 1, 0, '2020-02-21 17:31:29'),
(15, 2, '15', 2, 1, 0, '2020-02-21 17:31:29'),
(16, 1, '16', 1, 2, 0, '2020-02-21 17:31:29'),
(17, 1, '17', 1, 2, 0, '2020-02-21 17:31:29'),
(18, 1, '18', 1, 2, 0, '2020-02-21 17:31:29'),
(19, 1, '19', 1, 2, 0, '2020-02-21 17:31:29'),
(20, 1, '20', 1, 2, 0, '2020-02-21 17:31:29'),
(21, 1, '21', 1, 2, 0, '2020-02-21 17:31:29'),
(22, 1, '22', 1, 2, 0, '2020-02-21 17:31:29'),
(23, 1, '23', 1, 2, 0, '2020-02-21 17:31:29'),
(24, 1, '24', 1, 2, 0, '2020-02-21 17:31:29'),
(25, 1, '25', 1, 2, 0, '2020-02-21 17:31:29'),
(26, 1, '26', 1, 2, 0, '2020-02-21 17:31:29'),
(27, 1, '27', 1, 2, 0, '2020-02-21 17:31:29'),
(28, 1, '28', 1, 2, 0, '2020-02-21 17:31:29'),
(29, 1, '29', 1, 2, 0, '2020-02-21 17:31:29'),
(30, 2, '30', 2, 1, 0, '2020-02-21 17:31:29'),
(31, 2, '31', 2, 1, 0, '2020-02-21 17:31:29'),
(32, 2, '32', 2, 1, 0, '2020-02-21 17:31:29'),
(33, 2, '33', 2, 1, 0, '2020-02-21 17:31:29'),
(34, 2, '34', 2, 1, 0, '2020-02-21 17:31:29');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_roomtype`
--

CREATE TABLE `bsi_roomtype` (
  `roomtype_ID` int(10) NOT NULL,
  `type_name` varchar(500) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_roomtype`
--

INSERT INTO `bsi_roomtype` (`roomtype_ID`, `type_name`, `description`) VALUES
(1, 'Premiere Executive', 'The Executive rooms are designed to suit every need of the discerning business traveller. The rooms are done with stylish, contemporary dÃ©cor reflecting the premium standards of hospitality.\r\n\r\nExecutive rooms come with king sized beds, bathrooms with premium fittings & other exclusive features. Spacious & modern these rooms offer the perfect blend of luxury & convenience.'),
(2, 'Premiere Superior', 'The Premiere Superior rooms offers its guest a space to relax & feel at home. Elegantly furnished the rooms offers the business traveler all the comforts of a modern hotel.\r\n\r\nThe sleeping area has a queen sized bed & added amenities like in-room coffee & tea maker, round the clock service & high definition satellite television. '),
(3, 'Premiere Suite', 'The top of the line Premiere Suite immerses the guest in the highest standards of comfort & luxury. A personal sanctuary it creates the experience of living the big life.\r\n\r\nThe Premiere Suite is furnished with premium fittings & offers the very best of amenities. The Suite has a double sized king bed with feather soft mattress, ambient lighting & large sized LED TV.');

-- --------------------------------------------------------

--
-- Table structure for table `bsi_special_offer`
--

CREATE TABLE `bsi_special_offer` (
  `id` int(11) NOT NULL,
  `offer_title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `room_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `price_deduc` decimal(10,2) NOT NULL,
  `min_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bsi_special_offer`
--

INSERT INTO `bsi_special_offer` (`id`, `offer_title`, `start_date`, `end_date`, `room_type`, `price_deduc`, `min_stay`) VALUES
(1, 'Jan offer', '2020-01-27', '2020-01-31', '0', '10.00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bsi_admin`
--
ALTER TABLE `bsi_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_adminmenu`
--
ALTER TABLE `bsi_adminmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_advance_payment`
--
ALTER TABLE `bsi_advance_payment`
  ADD PRIMARY KEY (`month_num`);

--
-- Indexes for table `bsi_bookings`
--
ALTER TABLE `bsi_bookings`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `start_date` (`start_date`),
  ADD KEY `end_date` (`end_date`),
  ADD KEY `booking_time` (`discount_coupon`);

--
-- Indexes for table `bsi_capacity`
--
ALTER TABLE `bsi_capacity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_cc_info`
--
ALTER TABLE `bsi_cc_info`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `bsi_clients`
--
ALTER TABLE `bsi_clients`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `bsi_configure`
--
ALTER TABLE `bsi_configure`
  ADD PRIMARY KEY (`conf_id`);

--
-- Indexes for table `bsi_currency`
--
ALTER TABLE `bsi_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_email_contents`
--
ALTER TABLE `bsi_email_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_gallery`
--
ALTER TABLE `bsi_gallery`
  ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `bsi_invoice`
--
ALTER TABLE `bsi_invoice`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `bsi_language`
--
ALTER TABLE `bsi_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_payment_gateway`
--
ALTER TABLE `bsi_payment_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_priceplan`
--
ALTER TABLE `bsi_priceplan`
  ADD PRIMARY KEY (`plan_id`),
  ADD KEY `priceplan` (`roomtype_id`,`capacity_id`,`start_date`,`end_date`);

--
-- Indexes for table `bsi_reservation`
--
ALTER TABLE `bsi_reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bsi_room`
--
ALTER TABLE `bsi_room`
  ADD PRIMARY KEY (`room_ID`),
  ADD KEY `roomtype_id` (`roomtype_id`);

--
-- Indexes for table `bsi_roomtype`
--
ALTER TABLE `bsi_roomtype`
  ADD PRIMARY KEY (`roomtype_ID`);

--
-- Indexes for table `bsi_special_offer`
--
ALTER TABLE `bsi_special_offer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bsi_admin`
--
ALTER TABLE `bsi_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bsi_adminmenu`
--
ALTER TABLE `bsi_adminmenu`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `bsi_advance_payment`
--
ALTER TABLE `bsi_advance_payment`
  MODIFY `month_num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `bsi_capacity`
--
ALTER TABLE `bsi_capacity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bsi_clients`
--
ALTER TABLE `bsi_clients`
  MODIFY `client_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `bsi_configure`
--
ALTER TABLE `bsi_configure`
  MODIFY `conf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `bsi_currency`
--
ALTER TABLE `bsi_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bsi_email_contents`
--
ALTER TABLE `bsi_email_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bsi_gallery`
--
ALTER TABLE `bsi_gallery`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bsi_language`
--
ALTER TABLE `bsi_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `bsi_payment_gateway`
--
ALTER TABLE `bsi_payment_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bsi_priceplan`
--
ALTER TABLE `bsi_priceplan`
  MODIFY `plan_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `bsi_reservation`
--
ALTER TABLE `bsi_reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bsi_room`
--
ALTER TABLE `bsi_room`
  MODIFY `room_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `bsi_roomtype`
--
ALTER TABLE `bsi_roomtype`
  MODIFY `roomtype_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bsi_special_offer`
--
ALTER TABLE `bsi_special_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
