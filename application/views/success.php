<?php include_once "includes/header.php"; ?>

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Booking Success</h1>
            <ul class="breadcrumbs">
                <li>Home</a></li>
                <li class="active">Booking Success</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Booking flow start -->
<div class="booking-flow content-area-10">
    <div class="container">
        <h2 class="text-center">Booked successfully..!</h2>
        <h2 class="text-center"><small>Your Booking ID: #302112295143</small></h2>
        <section>           
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="<?php echo base_url();?>assets/img/success.jpg">
                </div>
            </div>                            
        </section>
    </div>
</div>
<!-- Booking flow end -->
<?php include_once "includes/footer.php"; ?>