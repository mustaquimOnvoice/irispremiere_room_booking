
<?php include_once "includes/header.php"; ?>
    <!-- Banner start -->
    <div class="banner banner-style-3 banner-max-height">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="<?php echo base_url();?>assets/img/banner/banner-slider-3.jpg" alt="banner-slider-3">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content banner-content-left text-left">
                            <h1 data-animation="animated fadeInDown delay-05s"><span>A WORLD OF LEISURE,<br></span> COMFORT & LUXURY</h1>
                            <p data-animation="animated fadeInUp delay-1s">Multi Cuisine Restaurants, Banquet, Spa & lots more</p>
                            <a href="#" class="btn btn-md btn-theme" data-animation="animated fadeInUp delay-15s">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="<?php echo base_url();?>assets/img/banner/banner-slider-2.jpg" alt="banner-slider-2">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content banner-content-left text-left">
                            <h1 data-animation="animated fadeInLeft delay-05s"><span>FOR THE MOST COMFORTABLE<br></span> STAY IN AHMEDNAGAR CITY</h1>
                            <p data-animation="animated fadeInUp delay-05s">Enjoy a world of unparalleled Comfort & Convenience at Hotel<br/> Iris Premiere </p>
                            <a href="#" class="btn btn-md btn-theme" data-animation="animated fadeInUp delay-15s">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="<?php echo base_url();?>assets/img/banner/banner-slider-1.jpg" alt="banner-slider-1">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content banner-content-left text-left">
                            <h1 data-animation="animated fadeInLeft delay-05s"><span>35 WELL APPOINTED ROOMS</h1>
                            <p data-animation="animated fadeInLeft delay-1s">World class amenities blended with traditional Indian hospitality</p>
                            <a href="#" class="btn btn-md btn-theme" data-animation="animated fadeInLeft delay-15s">Know More</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="slider-mover-left" aria-hidden="true">
                    <i class="fa fa-angle-left"></i>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="slider-mover-right" aria-hidden="true">
                    <i class="fa fa-angle-right"></i>
                </span>
                <span class="sr-only">Next</span>
            </a>

            <!-- Search area box start -->
            <div class="search-area-box-3 hidden-xs hidden-sm">
                <div class="search-contents">
                    <form method="get" id="myform" action="<?php echo site_url('booking');?>">
                        <div class="col-lg-12 col-pad col-pad-2">
                            <div class="search-your-rooms">
                                <h2 class="hidden-xs hidden-sm">Search Your <span>Rooms</span></h2>
                            </div>
                        </div>
                        <div class="search-your-details">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Check in: </label>
                                    <input type="text" name="check_in" required id="check_in" class="btn-default datepicker form-control" placeholder="Check In" value="" readonly required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Check out: </label>
                                    <input type="text" name="check_out" required id="check_out" class="btn-default datepicker form-control" placeholder="Check Out" value="" readonly required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Adult: </label>
                                    <select id="adults" class="selectpicker search-fields form-control-2 form-control" name="adults" value="" required>
                                        <option value="">Adult</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Child: </label>
                                    <select id="children" class="selectpicker search-fields form-control-2 form-control" name="children" value="" required>
                                        <option value="0">None</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <button name="btn_search" type="submit" id="btn_search" value="search" class="search-button btn-theme">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Search area box end -->
            
            <!-- Search area box 2 start -->
            <div class="search-area-box-2 hidden-lg hidden-md">
                <div class="container">
                    <div class="search-contents">
                        <form method="GET" id="myform" action="<?php echo site_url('booking');?>">
                            <div class="row">
                                <div class="col-md-3 col-pad col-pad-2">
                                    <div class="search-your-rooms">
                                        <h3 class="hidden-xs hidden-sm">Search</h3>
                                        <h2 class="hidden-xs hidden-sm">Your <span>Rooms</span></h2>
                                        <h2 class="hidden-lg hidden-md">Search Your <span>Rooms</span></h2>
                                    </div>
                                </div>
                                <div class="search-your-details">
                                    <div class="col-md-2 col-sm-4 col-xs-6 col-pad">
                                        <div class="form-group">
                                            <input type="text" name="check_in" required id="check_in" class="btn-default datepicker form-control" placeholder="Check In" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 col-pad">
                                        <div class="form-group">
                                            <input type="text" name="check_out" required id="check_out" class="btn-default datepicker form-control" placeholder="Check Out" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-4 col-xs-6 col-pad">
                                        <div class="form-group">
                                            <select id="adults" required class="selectpicker search-fields form-control-2 form-control" name="adults" value="">
                                                <option value="">Adult</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-4 col-xs-6 col-pad">
                                        <div class="form-group">
                                            <select id="children" required class="selectpicker search-fields form-control-2 form-control" name="children" value="">
                                                <option value="">Child</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-2 col-sm-4 col-xs-6 col-pad">
                                        <div class="form-group">
                                            <button name="btn_search" type="submit" id="btn_search" class="search-button btn-theme">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Search area box 2 end -->
        </div>
    </div>
    <!-- Banner end -->
<!-- Search area box 2 end -->
<?php include_once "includes/footer.php"; ?>

