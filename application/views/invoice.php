<?php include_once "includes/header.php"; ?>
<style>
    .top-header, .main-header, .main-footer, .copy-right {
        display: none;
    }
</style>
<!-- Booking flow start -->
<div class="booking-flow content-area-10">
    <div class="container">
        <section>
                <form id="contact_form" action="<?php echo site_url();?>Booking/booking_process" enctype="multipart/form-data" method="POST">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step3">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="contact-form sidebar-widget" style="text-align: center;">
                                        <h3 class="booking-heading-1">Booking Details</h3>
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                    <td><strong>Check In</strong></td>
                                                    <td><strong>Check Out</strong></td>
                                                    <td><strong>Total Nights</strong></td>
                                                    <td><strong>Total Rooms</strong></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $bookingDetails['check_in'] = $data['check_in'];?></td>
                                                    <td><?php echo $bookingDetails['check_out'] = $data['check_out'];?></td>
                                                    <td><?php echo $bookingDetails['total_nights'] = $data['total_nights'];?></td>
                                                    <td><?php echo $bookingDetails['total_romms'] = $data['total_romms'];?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                    <td><strong>No.</strong></td>
                                                    <td><strong>Room Type</strong></td>
                                                    <td><strong>Occupancy</strong></td>
                                                    <td><strong>Room Price</strong></td>
                                                    <td><strong>Rooms Selected</strong></td>
                                                    <td><strong>Total</strong></td>
                                                    <?php if(isset($data['billData'][0]['total_child_price'])) {?>
                                                        <td><strong>Child Price</strong></td>
                                                    <?php }?>
                                                    <td><strong>Net Total</strong></td>
                                                </tr>

                                                <?php $x=1; $colno=6; foreach($data['billData'] as $bill) {?>
                                                    <tr>
                                                        <td><?php echo $x;?></td>
                                                        <td><?php echo $bookingDetails[$x]['name'] = $bill['name'];?></td>
                                                        <td><?php echo $bookingDetails[$x]['maxOccup'] = $bill['maxOccup'];?></td>
                                                        <td><?php echo $bookingDetails[$x]['roomPrice'] = $bill['roomPrice'];?></td>
                                                        <td><?php echo $bookingDetails[$x]['svars_selectedrooms'] = $bill['svars_selectedrooms'];?></td>
                                                        <td><?php echo $bookingDetails[$x]['total'] = $bill['total'];?></td>
                                                        <?php if(isset($bill['total_child_price'])) { $colno=7;?>
                                                            <td><?php echo $bookingDetails[$x]['total_child_price'] = $bill['total_child_price'];?></td>
                                                        <?php }?>
                                                        <td><?php echo $bookingDetails[$x]['net_total'] = $bill['net_total'];?></td>
                                                    </tr>
                                                <?php $x++; }?>
                                                <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                    <td colspan="<?php echo $colno; ?>" style="text-align: right;">Gross Total</td>
                                                    <td>Rs <?php echo $bookingDetails['gross_total'] = $data['gross_total'];?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
        </section>
    </div>
</div>
<!-- Booking flow end -->
<?php include_once "includes/footer.php"; ?>