<?php include_once "includes/header.php"; ?>

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Payment Success</h1>
            <ul class="breadcrumbs">
                <li>Home</a></li>
                <li class="active">Payment Success</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Booking flow start -->
<div class="booking-flow content-area-10">
    <div class="container">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a>
                                <span class="round-tab">
                                    <i class="fa fa-folder-o"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Select Room</h3>
                        </li>
                        <li role="presentation">
                            <a >
                                <span class="round-tab">
                                    <i class="fa fa-cc"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Payment Info</h3>
                        </li>
                        <li role="presentation"  class="active">
                            <a href="#" data-toggle="tab" aria-controls="complete" role="tab" title="" data-original-title="Complete">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Success</h3>
                        </li>
                    </ul>
                </div>

                <form id="contact_form" action="<?php echo site_url();?>Booking/success" enctype="multipart/form-data" method="POST">
                    <div class="tab-content">
                       
                       
                        <div class="tab-pane active" role="tabpanel" id="complete">
                            <div class="booling-details-box booling-details-box-2 mrg-btm-30">
                                <h3 class="booking-heading-2">Booking Details</h3>
                                <div class="row mrg-btm-30">
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <!--  Rooms detail slider start -->
                                        <div class="rooms-detail-slider simple-slider ">
                                            <div id="carousel-custom-3" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-outer">
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-2.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-1.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-5.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-6.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-3.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item active">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-7.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                        <div class="item">
                                                            <img src="<?php echo base_url();?>assets/img/room/img-4.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                        </div>
                                                    </div>
                                                    <!-- Controls -->
                                                    <a class="left carousel-control" href="#carousel-custom-3" role="button" data-slide="prev">
                                                         <span class="slider-mover-left no-bg" aria-hidden="true">
                                                              <i class="fa fa-angle-left"></i>
                                                         </span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#carousel-custom-3" role="button" data-slide="next">
                                                         <span class="slider-mover-right no-bg" aria-hidden="true">
                                                              <i class="fa fa-angle-right"></i>
                                                         </span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Rooms detail slider end -->
                                        <p class="hidden-lg hidden-md">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel</p>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <h4>Your Payment ID: #302112295143</h4>

                                        <ul>
                                            <li>
                                                <span>Check In:</span> october 27, 2017
                                            </li>
                                            <li>
                                                <span>Check Out:</span> october 29, 2017
                                            </li>
                                            <li>
                                                <span>Adults:</span> 2
                                            </li>
                                            <li>
                                                <span>Child:</span> 1
                                            </li>
                                        </ul>

                                        <div class="your-address">
                                            <strong>Your Address:</strong>
                                            <address>
                                                <strong>John maikel</strong>
                                                <br><br>
                                                Level 13, 2 Elizabeth St, Melbourne
                                                Victoria 3000
                                            </address>
                                        </div>

                                        <div class="price">
                                            Total:$317.99
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 hidden-sm hidden-xs">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel</p>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <ul class="list-inline pull-right">
								<li><button type="button" onclick="goBack()" class="btn search-button btn-theme prev-step">Previous</button></li>
                                <li><button type="submit" name="btn_confirm" id="btn_confirm" class="btn search-button btn-theme next-step">confirm booking</button></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<!-- Booking flow end -->
<?php include_once "includes/footer.php"; ?>