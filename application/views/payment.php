<?php include_once "includes/header.php"; ?>

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Payment Info</h1>
            <ul class="breadcrumbs">
                <li>Home</a></li>
                <li class="active">Payment Info</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Booking flow start -->
<div class="booking-flow content-area-10">
    <div class="container">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <!--<div class="connecting-line"></div>-->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a >
                                <span class="round-tab">
                                    <i class="fa fa-folder-o"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Select Room</h3>
                        </li>
                        <li role="presentation" class="active">
                            <a href="#" data-toggle="tab" aria-controls="step3" role="tab" title="" data-original-title="Step 3">
                                <span class="round-tab">
                                    <i class="fa fa-cc"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Payment Info</h3>
                        </li>
                        <li role="presentation">
                            <a>
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Success</h3>
                        </li>
                    </ul>
                </div>

                <form id="contact_form" action="<?php echo site_url();?>Booking/booking_process" enctype="multipart/form-data" method="POST">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step3">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="contact-form sidebar-widget" style="text-align: center;">
                                            <!-- <h3 class="booking-heading-2">Booking Details</h3> -->
                                            <!--  Rooms detail slider start -->
                                            <!-- <div class="rooms-detail-slider simple-slider ">
                                                <div id="carousel-custom-2" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-outer">-->
                                                        <!-- Wrapper for slides -->
                                                        <!-- <div class="carousel-inner">
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-2.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-1.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-5.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-6.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-3.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-7.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                            <div class="item active">
                                                                <img src="<?php // echo base_url();?>assets/img/room/img-4.jpg" class="thumb-preview" alt="Chevrolet Impala">
                                                            </div>
                                                        </div>
                                                        <br>-->
                                                        <!-- Controls -->
                                                        <!--<a class="left carousel-control" href="#carousel-custom-2" role="button" data-slide="prev">
                                                            <span class="slider-mover-left no-bg" aria-hidden="true">
                                                                <i class="fa fa-angle-left"></i>
                                                            </span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-custom-2" role="button" data-slide="next">
                                                            <span class="slider-mover-right no-bg" aria-hidden="true">
                                                                <i class="fa fa-angle-right"></i>
                                                            </span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- Rooms detail slider end -->

                                            <h3 class="booking-heading-1">Booking Details</h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                        <td><strong>Check In</strong></td>
                                                        <td><strong>Check Out</strong></td>
                                                        <td><strong>Total Nights</strong></td>
                                                        <td><strong>Total Rooms</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $bookingDetails['check_in'] = $data['check_in'];?></td>
                                                        <td><?php echo $bookingDetails['check_out'] = $data['check_out'];?></td>
                                                        <td><?php echo $bookingDetails['total_nights'] = $data['total_nights'];?></td>
                                                        <td><?php echo $bookingDetails['total_romms'] = $data['total_romms'];?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                        <td><strong>No.</strong></td>
                                                        <td><strong>Room Type</strong></td>
                                                        <td><strong>Occupancy</strong></td>
                                                        <td><strong>Room Price</strong></td>
                                                        <td><strong>Rooms Selected</strong></td>
                                                        <td><strong>Total</strong></td>
                                                        <?php if(isset($data['billData'][0]['total_child_price'])) {?>
                                                            <td><strong>Child Price</strong></td>
                                                        <?php }?>
                                                        <td><strong>Net Total</strong></td>
                                                    </tr>

                                                    <?php $x=1; $colno=6; foreach($data['billData'] as $bill) {?>
                                                        <tr>
                                                            <td><?php echo $x;?></td>
                                                            <td><?php echo $bookingDetails[$x]['name'] = $bill['name'];?></td>
                                                            <td><?php echo $bookingDetails[$x]['maxOccup'] = $bill['maxOccup'];?></td>
                                                            <td><?php echo $bookingDetails[$x]['roomPrice'] = $bill['roomPrice'];?></td>
                                                            <td><?php echo $bookingDetails[$x]['svars_selectedrooms'] = $bill['svars_selectedrooms'];?></td>
                                                            <td><?php echo $bookingDetails[$x]['total'] = $bill['total'];?></td>
                                                            <?php if(isset($bill['total_child_price'])) { $colno=7;?>
                                                                <td><?php echo $bookingDetails[$x]['total_child_price'] = $bill['total_child_price'];?></td>
                                                            <?php }?>
                                                            <td><?php echo $bookingDetails[$x]['net_total'] = $bill['net_total'];?></td>
                                                        </tr>
                                                    <?php $x++; }?>
                                                    <tr style="background-color: rgb(63, 0, 79); color:white;">
                                                        <td colspan="<?php echo $colno; ?>" style="text-align: right;">Gross Total</td>
                                                        <td>Rs <?php echo $bookingDetails['gross_total'] = $data['gross_total'];?></td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <!-- <h4>Luxury Room</h4><ul>
                                                <li>
                                                    <span>Check In:</span> october 27, 2017
                                                </li>
                                                <li>
                                                    <span>Check Out:</span> october 29, 2017
                                                </li>
                                                <li>
                                                    <span>Rooms:</span> 3
                                                </li>
                                                <li>
                                                    <span>Adults:</span> 2
                                                </li>
                                                <li>
                                                    <span>Child:</span> 1
                                                </li>
                                            </ul> -->

                                            <!-- <div class="your-address">
                                                <strong>Your Address:</strong>
                                                <address>
                                                    <strong>John maikel</strong>
                                                    <br><br>
                                                    Level 13, 2 Elizabeth St, Melbourne
                                                    Victoria 3000
                                                </address>
                                            </div>

                                            <div class="price">
                                                Total:$317.99
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="contact-form sidebar-widget">
                                        <h3 class="booking-heading-2 black-color text-center">Billing Details</h3>
                                        <div class="row mb-30">
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <div class="form-group Country">
                                                    <label>Title</label>
                                                    <select required class="selectpicker country search-fields" name="title1" id="title1">
                                                        <option value="">Select</option>
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Ms.">Ms.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                        <option value="Miss.">Miss.</option>
                                                        <option value="Dr.">Dr.</option>
                                                        <option value="Prof.">Prof.</option>
                                                    </select>
                                                </div>												
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group fname">
                                                    <label>First Name</label>
                                                    <input type="hidden" required name="allowlang" id="allowlang" class="input-text" value="no">
                                                    <input type="text" required name="fname" id="fname" class="input-text" value="<?php echo set_value("fname");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group lname">
                                                    <label>Last Name</label>
                                                    <input type="text" required name="lname" id="lname" class="input-text" value="<?php echo set_value("lname");?>">
                                                </div>
                                            </div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group str_addr">
                                                    <label>Address</label>
													<input type="text" required name="str_addr" id="str_addr" class="input-text" value="<?php echo set_value("str_addr");?>">
                                                    <!-- <textarea name="address"  required id="address" class="input-text" ><?php // echo set_value("address");?></textarea> -->
													<!-- <input required type="checkbox" name="agree" id="agree" value="Agree"> I agree<br> -->
                                                </div>
                                            </div>
                                            
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group email">
                                                    <label>Email</label>
													<input type="email" required name="email" id="email" class="input-text" value="<?php echo set_value("email");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <div class="form-group city">
                                                    <label>City</label>
                                                    <input type="text" required name="city" id="city" class="input-text" value="<?php echo set_value("city");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <div class="form-group state">
                                                    <label>State</label>
                                                    <input type="text" required name="state" id="state" class="input-text" value="<?php echo set_value("state");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <div class="form-group zipcode">
                                                    <label>Zip/Post Code</label>
                                                    <input type="number" required name="zipcode" id="zipcode" class="input-text" value="<?php echo set_value("zipcode");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" hidden>
                                                <div class="form-group country">
                                                    <label>Country</label>
                                                    <input type="text" required name="country" id="country" class="input-text" value="India">
                                                </div>
                                            </div>
                                            
											
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group phone">
                                                    <label>Phone No.</label>
													<input type="text" required name="phone" id="phone" class="input-text" value="<?php echo set_value("phone");?>">
                                                </div>
                                            </div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group id_type">
                                                    <label>Identity Type</label>
													<input type="text" required name="id_type" id="id_type" class="input-text" value="<?php echo set_value("id_type");?>">
                                                </div>
                                            </div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group id_number">
                                                    <label>Identity Number</label>
													<input type="text" required name="id_number" id="id_number" class="input-text" value="<?php echo set_value("id_number");?>">
                                                </div>
                                            </div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group payment_type">
                                                    <label>Payment by</label><br>
                                                    <?php
                                                        foreach($data['paymentGatewayDetails'] as $key => $value){ 	
                                                            echo '<input type="radio" name="payment_type" id="payment_type_'.$key.'" value="'.$key.'" class="required" required/> '.$value['name'].' ';
                                                        }
                                                    ?>
                                                </div>
                                            </div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group message">
                                                    <label>Any additional requests</label>
													<input type="text" required name="message" id="message" class="input-text" value="<?php echo set_value("message");?>">
                                                </div>
                                            </div>											
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group tos">
                                                    <input required type="checkbox" name="tos" id="tos" value=""> <a href="<?php echo site_url('Booking/review')?>" target="_blank">I agree T&C.</a>
                                                    <input name="password"  id="password" type="hidden" class="input-large required" value="123456">
                                                    <input name="fax"  id="fax" type="hidden" class="input-large">
                                                    <input name="getVal"  id="getVal" type="hidden" class="input-large" value="<?php print_r($_GET);?>">
                                                    <input name="bookingDetails"  id="bookingDetails" type="hidden" class="input-large" value="<?php print_r($bookingDetails);?>">
                                                </div>
                                            </div>
                                        </div>

                                    <!--    <h3 class="booking-heading-2">Card Info</h3>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group phone">
                                                    <label>Card Number</label>
                                                    <input type="text" required name="card_number" id="card_number" class="input-text" value="<?php echo set_value("card_number");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group email">
                                                    <label>CVV</label>
                                                    <input type="text" required name="cvv" id="cvv" class="input-text" value="<?php echo set_value("cvv");?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group Country">
                                                    <label>Expire</label>
                                                    <select required class="selectpicker country search-fields" id="country" name="country"  >
                                                        <option value="">Month</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group Country">
                                                    <label>Year</label>
                                                    <select required class="selectpicker country search-fields" id="year" name="year" >
                                                        <option value="">Year</option>
                                                        <option value="1">2016</option>
                                                        <option value="2">2015</option>
                                                        <option value="3">2014</option>
                                                        <option value="4">2013</option>
                                                        <option value="5">2012</option>
                                                        <option value="6">2011</option>
                                                        <option value="7">2010</option>
                                                        <option value="8">2009</option>
                                                        <option value="9">2008</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                 -->   </div>
                                </div>
                                

                            <ul class="list-inline pull-right">
                                <li><button type="button" onclick="goBack()" class="btn search-button btn-theme prev-step">Previous</button></li>
                                <li><button type="submit" name="btn_payment" id="btn_payment" class="btn search-button btn-theme next-step">Save and continue</button></li>
                            </ul>
                        </div>
                            </div>
                </form>
            </div>
        </section>
    </div>
</div>
<!-- Booking flow end -->
<?php include_once "includes/footer.php"; ?>