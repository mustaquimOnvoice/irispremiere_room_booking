<?php include_once "includes/header.php"; ?>

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Available Rooms</h1>
            <ul class="breadcrumbs">
                <li>Home</li>
                <li class="active">Available Rooms</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Booking flow start -->
<div class="booking-flow content-area-10">
    <div class="container">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a data-toggle="tab" aria-controls="step1" role="tab" title="" data-original-title="Step 1" aria-expanded="false">
                                <span class="round-tab">
                                    <i class="fa fa-folder-o"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Select Room</h3>
                        </li>
                        <li>
                            <a>
                                <span class="round-tab">
                                    <i class="fa fa-cc"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Payment Info</h3>
                        </li>
                        <li>
                            <a>
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </span>
                            </a>
                            <h3 class="booking-heading">Success</h3>
                        </li>
                    </ul>
                </div>

                <form id="contact_form" action="<?php echo site_url('booking/payment');?>" enctype="multipart/form-data" method="GET" onSubmit="return validateSearchResultForm();">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="row">
							
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <!-- Search area box 2 start -->
                                    <div class="search-area-box-2 search-booking-box bg-grey">
                                        <div class="search-contents">
                                            <div class="search-your-rooms">
                                                <h3 class="hidden-xs hidden-sm">Search</h3>
                                                <h2 class="hidden-xs hidden-sm">Your <span>Rooms</span></h2>
                                                <h2 class="hidden-lg hidden-md">Search Your <span>Rooms</span></h2>
                                                <h5 class="hidden-xs hidden-md text-danger">No. of Nights : <?php echo $data['total_nights'];?></h5>
                                            </div>
											<!-- <form action="<?php // echo site_url('booking/payment');?>" id="booking_search_form"> -->
                                                <div class="search-your-details">
                                                    <div class="form-group">
                                                        <label>Check in: </label>
                                                        <input type="text" class="btn-default" name="check_in"  id="check_in" placeholder="Check In" value="<?php echo date('d/M/Y', strtotime($data['check_in']))?>" readonly required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Check out: </label>
                                                        <input type="text" class="btn-default" name="check_out"  id="check_out" placeholder="Check Out" value="<?php echo date('d/M/Y', strtotime($data['check_out']))?>" readonly required>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label>Total Adult: </label>
                                                        <input type="text" class="btn-default" name="adults"  id="adults" placeholder="Adults" value="<?php echo $data['adults']?>" readonly required>
                                                        <!-- <select class="selectpicker search-fields form-control-2" id="adults" name="adults">
                                                            <option value="<?php // echo $this->input->post('adults');?>"><?php // echo $this->input->post('adults');?></option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select> -->
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Total Children: </label>
                                                        <input type="text" class="btn-default" name="children"  id="children" placeholder="Children" value="<?php echo $data['children']?>" readonly required>
                                                        <!-- <select class="selectpicker search-fields form-control-2" id="children" name="children">
                                                            <option value="<?php // echo $this->input->post('children');?>"><?php // echo $this->input->post('children');?></option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select> -->
                                                    </div>
                                                    <div class="form-group">
                                                        <h5 class="hidden-xs hidden-md"><a href="<?php echo base_url();?>">(Modify search)</a></h5>
                                                    </div>

                                                    <!-- <div class="form-group">
                                                        <button type="submit" name="btn_search" id="btn_search" class="search-button btn-theme">Search</button>
                                                    </div> -->
                                                </div>
											<!-- </form> -->
                                        </div>
                                    </div>
                                    <!-- Search area box 2 end -->
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12">
									<!--<form action="<?php // echo base_url();?>index/payment">-->
                                    <div class="row">
                                        <!-- <div class="col-lg-12"><h3 class="booking-heading-2 black-color">Available Rooms</h3></div> -->
										<?php echo $data['availableRooms'];?>
                                    </div>
									<!--</form>-->
                                </div>
                            </div>
                            <ul class="list-inline pull-left">
                                <li><button type="submit" class="btn search-button btn-theme next-step">Save and continue</button></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<!-- Booking flow end -->
<script>
    function validateSearchResultForm(){
        var rmsavailable  = document.getElementsByName('svars_selectedrooms[]');	
        for(var i = 0; i < rmsavailable.length; i++){
            if(rmsavailable[i].value > 0){
                return true;
            }
        }		
        alert('Please select at least one room..!');
        return false;	
    } 
</script>
<?php include_once "includes/footer.php"; ?>