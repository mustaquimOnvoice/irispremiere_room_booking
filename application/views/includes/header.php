<!DOCTYPE html>
<html lang="zxx">

<head>
   <title>Iris Premiere</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/bootstrap-datepicker.min.css">
    


    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?php echo base_url();?>assets/css/skins/blue-light-2.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/572660eb138ce8c8524203a9_Favicon.png" type="image/png" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ie10-viewport-bug-workaround.css">

</head>
<body>

<div class="page_loader"></div>

<!-- Option Panel -->
<div class="option-panel option-panel-collased">
    <h2>Change Color</h2>
    <div class="color-plate sandybrown-plate" data-color="sandybrown"></div>
    <div class="color-plate blue-light-2-plate" data-color="blue-light-2"></div>
    <div class="color-plate yellow-plate" data-color="yellow"></div>
    <div class="color-plate red-plate" data-color="red"></div>
    <div class="color-plate green-light-plate" data-color="green-light"></div>
    <div class="color-plate purple-plate" data-color="purple"></div>
    <div class="color-plate blue-plate" data-color="blue"></div>
    <div class="color-plate peru-plate" data-color="peru"></div>
    <div class="color-plate green-plate" data-color="green"></div>
    <div class="color-plate blue-light-plate" data-color="blue-light"></div>
    <div class="color-plate green-light-2-plate" data-color="green-light-2"></div>
    <div class="color-plate royalblue-plate" data-color="royalblue"></div>
    
</div>
<!-- /Option Panel -->

<!-- Top header start -->
<header class="top-header top-header-3 hidden-xs" id="top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
                <div class="list-inline">
                    <a href="tel:1-8X0-666-8X88"><i class="fa fa-phone"></i>Online Promo Offer Get 10% Discount Use Coupon Code: IRIS10OFF</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                <ul class="social-list clearfix pull-right">
                    <li>
                        <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;+91 73500 01560 
                    </li>&nbsp;
                    <li>
                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;live@irispremiere.in
                    </li>
                   
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- Top header end -->

<!-- Main header start -->
<header class="main-header main-header-4">
    <div class="container">
        <nav class="navbar navbar-default">
		
		<a href="http://irispremiere.in/" class="logo">
                    <img src="<?php echo base_url();?>assets/img/logos/logo.png" alt="logo">
                </a>
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li class="dropdown active">
                        <a href="http://irispremiere.in/index.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Home</a></li>
					<li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Stay<span class="caret"></span>
                        </a>
						<ul class="dropdown-menu">
                            <li><a href="http://irispremiere.in/premiere-superior.php">Premiere Superior</a></li>
                            <li><a href="http://irispremiere.in/premiere-executive.php">Premiere Executive</a></li>
                            <li><a href="http://irispremiere.in/premiere-suite.php">Premiere Suite</a></li>
                        </ul>
                    </li>
					<li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Dinning<span class="caret"></span>
                        </a>
						<ul class="dropdown-menu">
                            <li><a href="http://irispremiere.in/elements.php">Elements</a></li>
                            <li><a href="http://irispremiere.in/ego.php">Ego</a></li>
                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="http://irispremiere.in/banquet.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Banquet</a>
                    </li>
					<li class="dropdown">
                        <a href="http://irispremiere.in/amenities.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Amenities</a>
                    </li>
					<li class="dropdown">
                        <a href="http://irispremiere.in/explore.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Explore</a>
                    </li>
					<li class="dropdown">
                        <a href="http://irispremiere.in/promotions.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Promotions</a>
                    </li>
					<li class="dropdown">
                        <a href="https://payments.djubo.com/accounts/QunGoSlR43_Sya8JvINUsQ/properties/7GyWwiQujEpY6HmLYwaBOQ/payment/custom-payment/" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Quick Pay</a>
                    </li>
					<li class="dropdown">
                        <a href="http://irispremiere.in/contact.php" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">Contact</a>
                    </li>
					  
                </ul>
               
            </div>

        </nav>

        <div class="header-search animated fadeInDown">
            <form class="form-inline">
                <input type="text" class="form-control" id="searchKey" placeholder="Search...">
                <div class="search-btns">
                    <button type="submit" name="btn_search" id="btn_search" class="btn btn-default">Search</button>
                </div>
            </form>
        </div>
    </div>
</header>
<!-- Main header end -->