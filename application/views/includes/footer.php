<!-- Footer start -->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer info-->
        <div class="footer-info">
            <div class="row">
                <!-- About us -->
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<div class="footer-item">
                        <div style="color:white;" class="main-title-2">
                            <h1>Contact Us</h1>
                        </div>
                        <ul class="personal-info">
                            <li>
                                <i class="fa fa-phone"></i>
                                +91 241 2327000
                            </li>
							<li>
                                <i class="fa fa-mobile"></i>
                                +91 9011137000
                            </li>
                            
                            <li>
                                <i class="fa fa-envelope"></i>
                                live@irispremiere.in
								</li>
                        </ul>
                        <div class="clearfix"></div>
                        <ul class="social-list">
                            <li><a href="https://twitter.com/irispremiere" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/irispremiere" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/irispremiere/" class="instagram-bg"><i class="fa fa-instagram"></i></a></li>
                            
                        </ul>
                    </div>
					</div>
					<div class="col-lg-2">
						<div class="footer-item">
                        <div class="main-title-2">
                            <h1>Link</h1>
                        </div>
                        <ul class="personal-info">
                            <li><a href="http://irispremiere.in/index.php">Home</a></li>
                            <li><a href="http://irispremiere.in/premiere-superior.php">Stay</a></li>
                            <li><a href="http://irispremiere.in/elements.php">Dinning</a></li>
                            <li><a href="http://irispremiere.in/banquet.php">Banquet</a></li>
                            <li><a href="http://irispremiere.in/amenities.php">Amenities</a></li>
                            
                        </ul>
                        <div class="clearfix"></div>
                    </div>
					</div>
					<div class="col-lg-2">
						<div class="footer-item">
                        <div class="main-title-2">
                            <h1>Link</h1>
                        </div>
                        <ul class="personal-info">
                            <li><a href="http://irispremiere.in/about.php">Home</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="http://irispremiere.in/explore.php">Explore</a></li>
                            <li><a href="http://irispremiere.in/promotions.php">Promotions</a></li>
                            <li><a href="http://irispremiere.in/contact.php">Contact</a></li>
                            
                        </ul>
                        <div class="clearfix"></div>
                    </div>
					</div>
					<div class="col-lg-3">
					<div class="footer-item gallery">
                        <div class="main-title-2">
                            <h1>Find Us</h1>
                        </div>
                        <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.598134760834!2d74.73033961437685!3d19.081398556762466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdcb0f7c80fffff%3A0xcddedd274f08707a!2sIris%20Premiere!5e0!3m2!1sen!2sin!4v1579779247826!5m2!1sen!2sin" width="300" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe></center>
                    </div></div>
					<div class="col-lg-1"></div>
				</div>
                
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        &copy;  2020 Developed by&nbsp;<a style="color:yellow;" href="http://onevoicetransmedia.com/" target="_blank">OneVoice Transmedia Pvt. Ltd.</a>
    </div>
</div>
<!-- Copy end right-->

<script>
    function goBack() {
        window.history.back();
    }
</script>
<script  src="<?php echo base_url();?>assets/js/jquery-2.2.0.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/bootstrap-submenu.js"></script>
<script  src="<?php echo base_url();?>assets/js/jquery.mb.YTPlayer.js"></script>
<script  src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/bootstrap-select.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
<script  src="<?php echo base_url();?>assets/js/jquery.scrollUp.js"></script>
<script  src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/jquery.filterizr.js"></script>
<script  src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/app.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script  src="<?php echo base_url();?>assets/js/ie10-viewport-bug-workaround.js"></script>

</body>

</html>