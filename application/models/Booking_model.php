<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking_model extends CI_Model 
{

	var $roomType = array();
	var $multiCapacity = array();
	var $mysqlCheckInDate = '';
	var $mysqlCheckOutDate = '';
	var $childPerRoom = 0;
	var $nightCount = 0;
	var $config = array();
	var $userDateFormat = "";
	var $svars_details = array();

	function bsiHotelCore(){		
		$this->getBSIConfig();
		$this->getUserDateFormat();		
	}

	private function getBSIConfig(){
		$rows = $this->db->query("SELECT conf_id, IFNULL(conf_key, false) AS conf_key, IFNULL(conf_value,false) AS conf_value FROM bsi_configure")->result_array();
		foreach($rows as $currentRow){
			if($currentRow["conf_key"]){
				if($currentRow["conf_value"]){
					$this->config[trim($currentRow["conf_key"])] = trim($currentRow["conf_value"]);
				}else{
					$this->config[trim($currentRow["conf_key"])] = false;
				}
			}
		}
	}

	private function getUserDateFormat(){		
		$dtformatter = array('dd'=>'%d', 'mm'=>'%m', 'yyyy'=>'%Y', 'yy'=>'%Y');		
		$dtformat = preg_split("@[/.-]@", $this->config['conf_dateformat']);
		$dtseparator = ($dtformat[0] === 'yyyy')? substr($this->config['conf_dateformat'], 4, 1) : substr($this->config['conf_dateformat'], 2, 1);
		$this->userDateFormat = $dtformatter[$dtformat[0]].$dtseparator.$dtformatter[$dtformat[1]].$dtseparator.$dtformatter[$dtformat[2]];	
	}

	function getDateRangeArray($startDate, $endDate, $nightAdjustment = true) {	
		$date_arr = array(); 
		$day_array=array(); 
		$total_array=array();
	     $time_from = mktime(1,0,0,substr($startDate,5,2), substr($startDate,8,2),substr($startDate,0,4));
		 $time_to = mktime(1,0,0,substr($endDate,5,2), substr($endDate,8,2),substr($endDate,0,4));		
		if ($time_to >= $time_from) { 
			if($nightAdjustment){
				while ($time_from < $time_to) {      
					array_push($date_arr, date('Y-m-d',$time_from));
					array_push($day_array, date('D',$time_from));
					$time_from+= 86400; // add 24 hours
				}
			}else{
				while($time_from <= $time_to) {      
					array_push($date_arr, date('Y-m-d',$time_from));
					array_push($day_array, $time_from);
					$time_from+= 86400; // add 24 hours
				}
			}			
		}  
		array_push($total_array, $date_arr);
		array_push($total_array, $day_array);
		return $total_array;		
	}

	function getRoomType() {
		$result = $this->db->get('bsi_roomtype');
		$rows = $result->result_array();
		$totRows = count($rows);
		
		for($i= 0; $i<$totRows; $i++){	
			array_push($this->roomType, array('rtid'=>$rows[$i]["roomtype_ID"], 'rtname'=>$rows[$i]["type_name"]));
		}
		return $this->roomType;
	}

	function getMultiCapacity($guestsPerRoom) {
		$this->db->where(array('capacity >=' => $guestsPerRoom));
		$result = $this->db->get('bsi_capacity');
		$rows = $result->result_array();
		$totRows = count($rows);

		for($i= 0; $i<$totRows; $i++){
			$capacity[$rows[$i]["id"]] = array('capval'=>$rows[$i]["capacity"],'captitle'=>$rows[$i]["title"]);
		}
		$this->multiCapacity = $capacity;
		return $this->multiCapacity;
	}

	function getAvailableRooms($roomTypeId, $roomTypeName, $capcityid, $check_in, $check_out, $children, $total_nights) {
		$this->mysqlCheckInDate = $check_in;
		$this->mysqlCheckOutDate = $check_out;
		$this->childPerRoom = $children;
		$this->nightCount = $total_nights;

		$searchresult = array('roomtypeid'=>$roomTypeId, 'roomtypename'=>$roomTypeName, 'capacityid'=>$capcityid, 'capacitytitle'=>$this->multiCapacity[$capcityid]['captitle'], 'capacity'=>$this->multiCapacity[$capcityid]['capval'], 'maxchild'=>$this->childPerRoom);
		$room_count = 0;
		$dropdown_html = '<option value="0" selected="selected">0</option>';
		$price_details_html = '';
		$total_price_amount = 0;
		$calculated_extraprice = 0;
		$total_specail_price=0;
		$specail_price_flag=0;
		$extraSearchParam = "";
		$minimum_night_flg=1;
		$variable_concat=1;
		$child_flag=false;

		$searchsql = "
					SELECT rm.room_ID, rm.room_no
					FROM bsi_room rm
					WHERE rm.roomtype_id = ".$roomTypeId."
					AND rm.capacity_id = ".$capcityid."".$extraSearchParam."
					AND rm.room_id NOT IN
						(SELECT resv.room_id
							FROM bsi_reservation resv, bsi_bookings boks
							WHERE boks.is_deleted = FALSE
								AND resv.bookings_id = boks.booking_id
								AND resv.room_type_id = ".$roomTypeId."
								AND (('".$this->mysqlCheckInDate."' BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY))
								OR (DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY) BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY)) 
								OR (boks.start_date BETWEEN '".$this->mysqlCheckInDate."' AND DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY)) 
								OR (DATE_SUB(boks.end_date, INTERVAL 1 DAY) 
								BETWEEN '".$this->mysqlCheckInDate."' AND DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY)
								))
						)";
						   
		$sql = $this->db->query($searchsql);
		$tmpctr = 1;
		$searchresult['availablerooms'] = array();
		$currentrow = $sql->result_array();
		$cntRows = count($currentrow);
		for($i= 0; $i<$cntRows; $i++){
			$dropdown_html.= '<option value="'.$tmpctr.'">'.$tmpctr.'</option>';
			array_push($searchresult['availablerooms'], array('roomid'=>$currentrow[$i]["room_ID"], 'roomno'=>$currentrow[$i]["room_no"]));
			$tmpctr++;
		}
		// echo '<pre>';
		// print_r($searchresult);
		// die();
		
		//mysql_free_result($sql);
		// $sql->close();
		
		if($tmpctr >= 1){
			$totalDays = $this->getDateRangeArray(date('Y-m-d',strtotime($this->mysqlCheckInDate)), date('Y-m-d',strtotime($this->mysqlCheckOutDate)));	
			$totalamt3=0;			
			 
			$dayName=array_count_values($totalDays[1]);
			$_month = date('M',strtotime($this->mysqlCheckInDate));
			$month_ = date('M',strtotime($this->mysqlCheckOutDate));
			$_color = '#f2f2f2';
			$color_ = '#f2f2f2';

			if($_month == $month_){
				$mon = $_month;
			}else{
				$mon = $_month.' - '.$month_;
			}

			$price_details_html='<tr><td bgcolor='.$_color.'><b>Month</b></td>';
			foreach($dayName as $days => $totalnum){
				$$days=0;
				$h1=$days.$variable_concat;
				$$h1=0;
			}

			 $total_child_price=0;
			 $total_child_price2=0;
			 $price_details_html.='<td bgcolor='.$color_.' align="right"><b>'.$this->nightCount.' Night(s)</b></td></tr>';
			 foreach($totalDays[0] as $date2 => $val){	
				
				$pricesql = $this->db->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id = ".$capcityid." AND ('".$val."' BETWEEN start_date AND end_date)");
				// $pricesql = $mysqli->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id = ".$capcityid." AND ('".$val."' BETWEEN start_date AND end_date)");
				if($pricesql->num_rows()){
					$row=$pricesql->row_array();
					// $row=$pricesql->fetch_assoc();
				}else{
					$pricesql2 = $this->db->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id = ".$capcityid." AND  default_plan=1");
					$row=$pricesql2->row_array();
					// $pricesql2 = $mysqli->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id = ".$capcityid." AND  default_plan=1");
					// $row=$pricesql2->fetch_assoc();
				}
				
				$day=date('D',strtotime($val));
				$$day+=$row[strtolower($day)];

				//*************************** specail offer ******************
				$sql_sp = $this->db->query("select * from bsi_special_offer where '".$val."' between  `start_date` and `end_date` and (room_type=".$roomTypeId ." or room_type=0)");
				$row99 = $sql_sp->row_array();
				// $row99=$sql_sp->fetch_assoc();
				
				$h2=$day.$variable_concat;
					if($sql_sp->num_rows()){				
						$c999=round($row[strtolower($day)]- (($row[strtolower($day)]*$row99['price_deduc'])/100),1);
						$$h2+=$c999;
						if($row99['min_stay'] <= $minimum_night_flg){ $specail_price_flag=1; }					
						$minimum_night_flg++;					
					}else{
						$$h2+=$row[strtolower($day)];
					}
				
				//echo $$day.'/'.$$h2.' ';

				//*************************** specail offer ******************
				// $csql=$mysqli->query("SELECT distinct(`no_of_child`) FROM `bsi_room` WHERE `capacity_id`=".$capcityid." and `roomtype_id`=".$roomTypeId."");
				// $chld_row2=$csql->fetch_assoc();
				$csql = $this->db->query("SELECT distinct(`no_of_child`) FROM `bsi_room` WHERE `capacity_id`=".$capcityid." and `roomtype_id`=".$roomTypeId."");
				$chld_row2 = $csql->row_array();
				if($chld_row2['no_of_child'] >= $this->childPerRoom && $this->childPerRoom != 0){
					$child_flag=true;
					$childpricesql = $this->db->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id =1001 AND ('".$val."' BETWEEN start_date AND end_date)");
					// $childpricesql = $mysqli->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id =1001 AND ('".$val."' BETWEEN start_date AND end_date)");
					 if($pricesql->num_rows()){
						$chrow=$childpricesql->row_array();
						// $chrow=$childpricesql->fetch_assoc();
				     }else{
						$childpricesql2 = $this->db->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id =1001 AND  default_plan=1");
						$chrow = $childpricesql2->row_array();
						// $childpricesql2 = $mysqli->query("SELECT * FROM bsi_priceplan WHERE roomtype_id = ".$roomTypeId." AND capacity_id =1001 AND  default_plan=1");
						// $chrow=mysqli_fetch_assoc($childpricesql2);
				      }
					  
					  $day = date('D',strtotime($val));					
					  $$day+=($chrow[strtolower($day)]*$this->childPerRoom); 

					  //*************************** specail offer ******************
						$sql_sp1 = $this->db->query("select * from bsi_special_offer where '".$val."' between  `start_date` and `end_date` and (room_type=".$roomTypeId ." or room_type=0)");
						// $sql_sp1=$mysqli->query("select * from bsi_special_offer where '".$val."' between  `start_date` and `end_date` and (room_type=".$roomTypeId ." or room_type=0)");						
						
						//$h22=$day.$variable_concat;
							if($sql_sp1->num_rows()){
								$row999 = $sql_sp1->row_array();						
								// $row999=$sql_sp1->fetch_assoc();						
								$c999 = round(($chrow[strtolower($day)]*$this->childPerRoom)- ((($chrow[strtolower($day)]*$this->childPerRoom)*$row999['price_deduc'])/100),1);
								//echo $c999;
								$$h2+=$c999;
								$total_child_price2+=$c999;								
							}else{
								$$h2+=($chrow[strtolower($day)]*$this->childPerRoom);
								$total_child_price2+=($chrow[strtolower($day)]*$this->childPerRoom); 
							}
						
						//echo $$day.'/'.$$h2.' ';

						//*************************** specail offer ******************			  
					$total_child_price+=($chrow[strtolower($day)]*$this->childPerRoom); 					  
				}
			}

			$night_count_at_customprice = 0;	
			$searchresult['prices'] = array();	
							
				foreach($dayName as $days => $totalnum){				  
				   $totalamt3=$totalamt3+$$days;	
				   $h3=$days.$variable_concat;
				   $total_specail_price=$total_specail_price+$$h3;
				} 
			
			 $total_price_amount=$totalamt3;
			// echo $total_specail_price;
			$conf_tax_amount = $this->db->where(array('conf_key' => 'conf_tax_amount'))->get('bsi_configure')->row()->conf_value;
			$conf_price_with_tax = $this->db->where(array('conf_key' => 'conf_price_with_tax'))->get('bsi_configure')->row()->conf_value;
			
			if($conf_tax_amount > 0 && $conf_price_with_tax==1) {
				$total_price_amount=$total_price_amount+(($total_price_amount * $bsiCore->config['conf_tax_amount'])/100);
				$total_specail_price=$total_specail_price+(($total_specail_price * $bsiCore->config['conf_tax_amount'])/100);
				$total_child_price=$total_child_price+(($total_child_price * $bsiCore->config['conf_tax_amount'])/100);
				$total_child_price2=$total_child_price2+(($total_child_price2 * $bsiCore->config['conf_tax_amount'])/100);
			}			
		}

		if($specail_price_flag){
			$searchresult['roomprice'] = $total_specail_price;	
		}else{
			$searchresult['roomprice'] = $total_price_amount;	
		}
		// echo '<pre>';
		// print_r($searchresult);
		// die();
		if($tmpctr > 1) array_push($this->svars_details, $searchresult);
		$this->session->set_userdata('svars_details',$this->svars_details);
		unset($searchresult);
		
		return array('roomcnt' => $tmpctr-1,
					'roomdropdown' => $dropdown_html,
					'pricedetails' => $price_details_html,
					'child_flag'=>$child_flag,
					'specail_price_flag'=>$specail_price_flag,
					'total_specail_price'=>$total_specail_price,
					'total_child_price'=>$total_child_price,
					'total_child_price2'=>$total_child_price2,
					'totalprice' => $total_price_amount ); 
	}

	public function clearExpiredBookings(){
		$rows = $this->db->query("SELECT booking_id FROM bsi_bookings WHERE payment_success = false AND ((NOW() - booking_time) > ".intval($this->config['conf_booking_exptime'])." )")->result_array();
		$i= 0;
		foreach($rows as $currentRow){
			$this->db->query("DELETE FROM bsi_invoice WHERE booking_id = '".$currentRow["booking_id"]."'");
			$this->db->query("DELETE FROM bsi_reservation WHERE bookings_id = '".$currentRow["booking_id"]."'");	
			$this->db->query("DELETE FROM bsi_bookings WHERE booking_id = '".$currentRow["booking_id"]."'");			
		}
	}

	public function loadPaymentGateways() {
		$paymentGateways = array();
		$rows = $this->db->query("SELECT * FROM bsi_payment_gateway where enabled=true")->result_array();
		foreach($rows as $currentRow){
			$paymentGateways[$currentRow["gateway_code"]] = array('name'=>$currentRow["gateway_name"], 'account'=>$currentRow["account"]);	 
		}
		return $paymentGateways;
	}


	//-- config file --//
	public function getMySqlDate($date){
		if($date == "") return "";
		$dateformatter = preg_split("@[/.-]@", $this->config['conf_dateformat']);
		$date_part = preg_split("@[/.-]@", $date);		
		$date_array = array();		
		for($i=0; $i<3; $i++) {
			$date_array[$dateformatter[$i]] = $date_part[$i];
		}
		return $date_array['yy']."-".$date_array['mm']."-".$date_array['dd'];
	}

	public function ClearInput($dirty){
		$dirty =$this->db->escape($dirty);
		return $dirty;
	}

	public function capacitycombo(){
		$chtml = '<select id="capacity" name="capacity" class="input-medium">';
		$sql=$this->db->query("SELECT Max(capacity) as capa FROM bsi_capacity WHERE `id` IN (SELECT DISTINCT (capacity_id) FROM bsi_room) ORDER BY capacity")->result_array();
		$capacityrow = $sql[0];
			for($i=1; $i<=$capacityrow["capa"]; $i++){ 
				$chtml .=  '<option value="'.$i.'">'.$i.'</option>';
			}
		$chtml .= '</select>';	
		return $chtml;
	}

	public function encryptCard($creditno){
		$key = 'sdj*sadt63423h&%$@c34234c346v4c43czxcx'; //Change the key here
		$td = mcrypt_module_open('tripledes', '', 'cfb', '');
		srand((double) microtime() * 1000000);
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		$okey = substr(md5($key.rand(0, 9)), 0, mcrypt_enc_get_key_size($td));
		mcrypt_generic_init($td, $okey, $iv);
		$encrypted = mcrypt_generic($td, $creditno.chr(194));
		$code = $encrypted.$iv;
		$code = eregi_replace("'", "\'", $code);
		return $code;
	}
	
	public function decryptCard($code){
		$key = 'sdj*sadt63423h&%$@c34234c346v4c43czxcx'; // use the same key used for encrypting the data
		$td = mcrypt_module_open('tripledes', '', 'cfb', '');
		$iv = substr($code, -8);
		$encrypted = substr($code, 0, -8);
		for ($i = 0; $i < 10; $i++) {
			$okey = substr(md5($key.$i), 0, mcrypt_enc_get_key_size($td));
			mcrypt_generic_init($td, $okey, $iv);
			$decrypted = trim(mdecrypt_generic($td, $encrypted));
			mcrypt_generic_deinit($td);
			$txt = substr($decrypted, 0, -1);
			if (ord(substr($decrypted, -1)) == 194 && is_numeric($txt)) break;
		}
		mcrypt_module_close($td);
		return $txt;
	}
	
	public function paymentGateway($code){
		return $this->db->query("SELECT gateway_name FROM bsi_payment_gateway where gateway_code='".$code."'")->row()->gateway_name;
	}

	public function getInvoiceinfo($bid){
		return $this->db->query("select * from bsi_invoice where booking_id='".$bid."'")->row()->invoice;
	}

	public function paymentGatewayName($gcode){
		return $this->db->query("SELECT gateway_name FROM bsi_payment_gateway where gateway_code='".$gcode."'")->row()->gateway_name;
	}

	public function getChildcombo(){
		$mchild= $this->db->query("SELECT max(`no_of_child`) as mchild FROM `bsi_room`")->row()->mchild;
		$childhtml="";
		if($mchild){
			$childhtml.= '<div class="control-group">
                            <label class="control-label" for="checkInDate">Child per room:</label>
                            <div class="controls">
                            	<select class="input-medium" id="child_per_room" name="child_per_room"><option value="0" selected>Select</option>' ;		
								for($k=1;$k<=$mchild;$k++){
									$childhtml.='<option value="'.$k.'">'.$k.'</option>';
								}
								$childhtml.=' </select></div></div>';
		}else{
			$childhtml ='<input type="hidden" name="child_per_room" value="0"/>';
		}		
		return $childhtml;
	}

	public function getExchangemoney_update() {
		$sql=$this->db->query("select * from bsi_currency where default_c = 0")->result_array();
		$default2=$this->db->query("select * from bsi_currency where default_c = 1")->result_array();
		
		$i= 0;
		foreach($sql as $row){
			$amount=1;
			$amount = urlencode($amount);
			$from_Currency = urlencode($default2[$i]['currency_code']);
			$to_Currency = urlencode($row['currency_code']);
			$url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";
			$ch = curl_init();
			$timeout = 0;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_USERAGENT,
			"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$rawdata = curl_exec($ch);
			curl_close($ch);
			$data = explode('bld>', $rawdata);  
			$data = explode($to_Currency, $data[1]);
			$var=round($data[0], 2);
		//return round($var,3);
			$this->db->query("update bsi_currency set  exchange_rate ='".$var."' where currency_code='".$row['currency_code']."'");
			$i++;
		}
   }

   	public function getExchangemoney($amount1,$to_Currency1){
		$row=$this->db->query("select * from bsi_currency where currency_code = '".$to_Currency1."'")->row();
		$exchange_rate=$row->exchange_rate;
		$amount        = $amount1*$exchange_rate;
		return number_format($amount,2);
	}

	public function currency_symbol(){
		return $this->db->query("select * from bsi_currency where default_c = 1")->row()->currency_symbl;
	}

	public function currency_code(){
		return $this->db->query("select * from bsi_currency where default_c = 1")->row()->currency_code;
	}
	
	public function get_currency_symbol($c_code){
		return $this->db->query("select * from bsi_currency where currency_code = '".$c_code."'")->row()->currency_symbl;
	}

	public function get_currency_combo3($c_code){
		$sql=$this->db->query("select * from bsi_currency order by currency_code")->result_array();
		$combo='<div class="control-group">
						  <label class="control-label" for="checkInDate">CURRENCY:</label>
						  <div class="controls">
							  <select class="input-medium" name="currency" id="currency">';
								foreach($sql as $row){
									if($row['currency_code'] == $c_code)
										$combo.='<option value="'.$row["currency_code"].'"  selected="selected">'.$row['currency_code'].'</option>';
									else
										$combo.='<option value="'.$row["currency_code"].'">'.$row['currency_code'].'</option>';
								}
								$combo.='</select>
						  </div>
					  </div>';
		if(count($sql) == 1){
			$combo='<input type="hidden" name="currency" value="'.$this->currency_code().'" />';
		}		
		return $combo;
	}
	
	public function get_currency_combo2($c_code){
		$combo='<select name="currency"  class="input-small" onchange="currency_change(this.value)">';
			$sql=$this->db->query("select * from bsi_currency order by currency_code")->result_array();
				foreach($sql as $row){
					if($row['currency_code'] == $c_code)
						$combo.='<option value="'.$row["currency_code"].'"  selected="selected">'.$row['currency_code'].'</option>';
					else
						$combo.='<option value="'.$row["currency_code"].'">'.$row['currency_code'].'</option>';
				}		 
		$combo.='</select>';
		if(count($sql) == 1){
			$combo='';
		}
		return $combo;
	}

	public function exchange_rate_update($type=1){
		if($type){
			if($this->config['conf_currency_update_time']!=''){
				if(time() > ($this->config['conf_currency_update_time'] + 12 * 3600)){
					 $this->getExchangemoney_update();
					 $this->db->query("update bsi_configure set conf_value='".time()."' where conf_key='conf_currency_update_time'");
				}
			}else{
				$this->getExchangemoney_update();
				$this->db->query("update bsi_configure set conf_value='".time()."' where conf_key='conf_currency_update_time'");
			}		
		}else{
			$this->getExchangemoney_update();
		}
		
	}

	public function roomtype_photos($rid,$cid){
		$sql=$this->db->query("select * from bsi_gallery where roomtype_id=".$rid." and capacity_id=".$cid)->result_array();
		$list_img='';
		$lbox='';
		if(count($sql) >= 1){
			foreach($sql as $row){
				$list_img.='<li><a class="group_'.$rid.'_'.$cid.'" href="gallery/'.$row['img_path'].'" style="text-decoration:none; " ><img src="gallery/thumb_'.$row['img_path'].'" style="border-style: none" /></a></li>';			
			}
		}else{
			 $list_img.='<li><img src="images/no_photo.jpg" /></li>';
		}		
		return $list_img;
	}

	public function roomtype_photos_url($rid,$cid){
		$sql=$this->db->query("select * from bsi_gallery where roomtype_id=".$rid." and capacity_id=".$cid)->result_array();
		$list_img= array();
		$list_img_thumb= array();
		$lbox='';
		if(count($sql) == 0){
			array_push($list_img,'images/no_photo.jpg');
			array_push($list_img_thumb,'images/no_photo.jpg');
		}else{
			foreach($sql as $row){
				$img_path = $row['img_path'];
				array_push($list_img,"$img_path");
				array_push($list_img_thumb,"thumb_$img_path");
			}
		}	
		return array('list_img' => $list_img, 'list_img_thumb' => $list_img_thumb);
	}
	
	public function bt_date_format(){
		if($this->config['conf_dateformat']=='yy-mm-dd')
			$df='yy'.$this->config['conf_dateformat'];
		else
		 	$df=$this->config['conf_dateformat'].'yy';
		 
		return $df;
	}

}