<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	var $check_in;
	var $check_out;
	var $adults;
	var $children;
	var $total_nights;
	var $paymentGatewayDetails;
	var $total_romms;
	var $gross_total;
	var $billData;

	public function __construct()
	{
		parent::__construct();		
		$this->load->model('Booking_model','bmodel');
		$this->bmodel->bsiHotelCore();		
	}

	public function index()
	{
		if(empty($_GET['check_in']) || empty($_GET['check_in']) || empty($_GET['check_in']) || empty($_GET['check_in']) || empty($_GET['btn_search'])) {
			redirect(base_url()); // REDIRECT TO HOME IF ANY THING IS MISSING
		}

		$date1 = new DateTime($_GET['check_in']);
		$date2 = new DateTime($_GET['check_out']);
		$numberOfNights= $date2->diff($date1)->format("%a"); // this calculates the diff between two dates, which is the number of nights
		
		$this->check_in = trim($_GET['check_in']);
		$this->check_out = trim($_GET['check_out']);
		$this->adults = trim($_GET['adults']);
		$this->children = trim($_GET['children']);
		$this->total_nights = $numberOfNights;
		
		// availables rooms
		$gotSearchResult = false;
		$idgenrator = 0;
		$ik=1;
		$roomTypes = '';
		$multiCapacitys = '';
		$totRomms = $this->bmodel->getRoomType();
		$totMultiCapacity = $this->bmodel->getMultiCapacity($this->adults);
		$availableRooms = '';
		$availableRooms .= '<input type="hidden" name="checkinDatetime" value="'.$_GET['check_in'].'">';
		$availableRooms .= '<input type="hidden" name="checkoutDatetime" value="'.$_GET['check_out'].'">';
		foreach($totRomms as $room_type){
			foreach($totMultiCapacity as $capid => $capvalues){
				$room_result = $this->bmodel->getAvailableRooms($room_type['rtid'], $room_type['rtname'], $capid, $this->check_in, $this->check_out, $this->children, $this->total_nights);
				
				// $sqlroomcheck=$mysqli->query("select * from bsi_room where roomtype_id=".$room_type['rtid']." and capacity_id=".$capid);
				$sqlroomcheck = $this->db->query("select * from bsi_room where roomtype_id=".$room_type['rtid']." and capacity_id=".$capid);

				if($sqlroomcheck->num_rows()){
					// $availableRooms = '<script> $(document).ready(function() {
					// 	$("#iframe_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: $(\'#body-div\').width() + \'px\', height: "90%"});
					// 	$("#iframe_details_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true,  width: $(\'#body-div\').width() + \'px\', height: "60%"}); 
					// 	$(".group_'.$room_type['rtid'].'_'.$capid.'").colorbox({rel:\'group_'.$room_type['rtid'].'_'.$capid.'\', maxWidth: $(\'#body-div\').width(), maxHeight:"80%", slideshow:true, slideshowSpeed:5000});}); </script><script type="text/javascript">
					// 		$(document).ready(function() {
					// 		$("#mySlides_'.$capid.'_'.$room_type['rtid'].' a").lightBox();
					// 	});
					// </script>';

					$child = '';
					if($room_result["child_flag"]) {
						$child =' with Child';
					}
					$roomNames = $room_type["rtname"].' ('.$capvalues["captitle"].')'.$child;

					$childPeRoom = '';
					if($room_result['child_flag']){
						$childPeRoom = ' & '.$this->children .' Child per Room';
					}
					$maxOccup = $capvalues["capval"].' Adult'.$childPeRoom;
					$totalprice = $room_result['totalprice'];

					$roomPhoto = $this->bmodel->roomtype_photos_url($room_type['rtid'],$capid);
					$img = $roomPhoto['list_img'][0];

					$availableRooms .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="hotel-box">
											<div class="header clearfix">
												<img src='.base_url("assets/gallery/$img").' alt="img-1" class="img-responsive">
											</div>
											
											<div class="detail clearfix">
												<h3><a href="#">'.$roomNames.'</a></h3>
												<table  width="100%" class="table">
													<tr> 
														<td><strong>Max Occupancy</strong></td>
														<td>'.$maxOccup.'</td>
													</tr>
													<tr>
														<td>
															<strong> Total Price / Room </strong>
														</td>';

														if($room_result['specail_price_flag']){

					$availableRooms .=						'<td>
																<span style="font-weight:bold; color:#cc0000;">
																	<del>
																		Rs.'.$totalprice.'
																	</del>
																</span>
																<strong>
																	Rs.'.$room_result['total_specail_price'].'
																</strong>';
																if($room_result['child_flag']){
					$availableRooms .=								'(included <span style="color:#cc0000; text-decoration:line-through;">
																		Rs.'.$room_result['total_child_price'].'
																	</span> 
																		Rs.'.$room_result['total_child_price2'].' for '.$this->children .'Child)
																		<input type="hidden" name="total_child_price[]" value="'.$room_result['total_child_price'].'">';
																}
					$availableRooms .=						'</td>';

														}else{
					$availableRooms .=						'<td>
																<span style="font-weight:bold;">
																<strong>
																	Rs.'.$totalprice.'
																</strong>';
																if($room_result['child_flag']){
					$availableRooms .=								'(included Rs.'.$room_result['total_child_price'].' for '.$this->children .'Child)
																	<input type="hidden" name="total_child_price[]" value="'.$room_result['total_child_price'].'">';
																}
					$availableRooms .=						'</td>';
														}
					$availableRooms .=				'</tr>
													<tr>';																
														if(intval($room_result['roomcnt']) > 0){ $gotSearchResult = true;
					$availableRooms .=						'<td><strong>Select No. of Room</strong></td>
															<td>
																<select name="svars_selectedrooms[]" id="room"  class="btn-theme form-control" name="room" value="" required">
																	'.$room_result['roomdropdown'].'
																</select>
															</td>';
														}else{
					$availableRooms .=					'<td colspan="2">
																<strong>Not Available</strong>
														</td>';
														} 
					$availableRooms .=				'</tr>
													<tr>
														<input type="hidden" name="roomNames[]" value="'.$roomNames.'">
														<input type="hidden" name="maxOccup[]" value="'.$maxOccup.'">
														<input type="hidden" name="roomPrice[]" value="'.$totalprice.'">
													</tr>
												</table>
											</div>
										</div>								
									</div>';
				}
			}
		}
		//End availables rooms

		$pagedata['data'] = array(
			'check_in' => $this->check_in,
			'check_out' => $this->check_out,
			'adults' => $this->adults,
			'children' => $this->children,
			'total_nights' => $this->total_nights,
			'availableRooms' => $availableRooms
		);

		// echo '<pre>';
		// print_r($this->session->userdata());
		// die();
		
		$this->load->view('booking_system',$pagedata);
		// $availableRooms = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		// 					<div class="hotel-box">
		// 						<!--header -->
		// 						<div class="header clearfix">
		// 							<img src='.base_url("assets/img/room/img-1.jpg").' alt="img-1" class="img-responsive">
		// 						</div>
								
		// 						<div class="detail clearfix">
		// 							<h3>
		// 								<a href="rooms-details.html">Luxury Room</a>
		// 							</h3>
		// 							<h5 class="location">
		// 								<a href="rooms-details.html">
		// 									<i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
		// 								</a>
		// 							</h5>
		// 							<p>Lorem ipsum dolor sit amet, conser adipisic elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		// 							<br/>
		// 							<div>
		// 								<select id="room"  class="selectpicker btn-theme search-fields form-control-2 form-control" name="room" value="" required>
		// 									<option value="">Room</option>
		// 									<option value="1">1</option>
		// 									<option value="2">2</option>
		// 									<option value="3">3</option>
		// 								</select>
		// 							</div>
		// 						</div>
		// 					</div>
		// 				  </div>';
	}

	public function booking_system()
	{
		$this->load->view('booking_system');
	}

	public function payment()
	{
		$this->bmodel->clearExpiredBookings();
		$date1 = new DateTime($_GET['checkinDatetime']);
		$date2 = new DateTime($_GET['checkoutDatetime']);
		$numberOfNights= $date2->diff($date1)->format("%a"); // this calculates the diff between two dates, which is the number of nights
		
		$this->check_in = trim($_GET['check_in']);
		$this->check_out = trim($_GET['check_out']);
		$this->adults = trim($_GET['adults']);
		$this->children = trim($_GET['children']);

		$svars_selectedrooms = $_GET['svars_selectedrooms'];
		$roomNames = $_GET['roomNames'];
		$roomPrice = $_GET['roomPrice'];
		$maxOccup = $_GET['maxOccup'];		

		// $total = array();
		// foreach ($roomPrice as $key=>$price) {
		// 	$total[] = $price * $svars_selectedrooms[$key];
		// }

		$billData = array();
		foreach ($roomNames as $key=>$name) {
			if($svars_selectedrooms[$key] != 0){ //remove not selected rooms
				$billData[] =array('name'=>$name, 'maxOccup'=> $maxOccup[$key], 'roomPrice'=> $roomPrice[$key], 'svars_selectedrooms'=> $svars_selectedrooms[$key], 'total'=> $svars_selectedrooms[$key] * $roomPrice[$key], 'net_total'=> $svars_selectedrooms[$key] * $roomPrice[$key]);
			}
		}
		
		if(isset($_GET['total_child_price'])){
			$total_child_price = $_GET['total_child_price'];
			foreach ($total_child_price as $key=>$name) {
				if($svars_selectedrooms[$key] != 0){ //remove not selected rooms
					$total_child_price_merge = array('total_child_price'=> $total_child_price[$key],'net_total'=> $total_child_price[$key]+$billData[$key]['total']);
					$billData2[] = array_merge($billData[$key],$total_child_price_merge);
				}
			}	
			$billData =	$billData2;
		}

		$this->paymentGatewayDetails = $this->bmodel->loadPaymentGateways();
		$this->total_nights = $numberOfNights;
		$this->total_romms = array_sum($_GET['svars_selectedrooms']);
		$this->gross_total = array_sum(array_column($billData,'net_total'));
		$this->billData = $billData;

		$pagedata['data'] = array(
			'check_in' => $this->check_in,
			'check_out' => $this->check_out,
			'adults' => $this->adults,
			'children' => $this->children,
			'total_nights' => $this->total_nights,
			'total_romms' => $this->total_romms,
			'gross_total' => $this->gross_total,
			'billData' => $this->billData,
			'paymentGatewayDetails' => $this->paymentGatewayDetails
		);

		// echo '<pre>';
		// print_r($this->session->userdata());
		// print_r($_GET);
		// print_r($_POST);
		// print_r($pagedata['data']);
		// die();
		$this->load->view('payment',$pagedata);
	}

	public function booking_process(){	
		
		//includes files		
		$row_default_lang=$this->db->query("select lang_file from bsi_language where `lang_default`=true")->row()->lang_file;
		require_once APPPATH . 'core/languages/'.$row_default_lang; //Load languages
		
		// $pos2 = strpos($_SERVER['HTTP_REFERER'],$_SERVER['SERVER_NAME']);
		// if(!$pos2){
			// 	header('Location: booking-failure.php?error_code=9'); // redirect to fail
			// }
		
		if(empty($_POST)){
			//remove session and redirect to home 
		}
		echo '<pre>';
		print_r($pagedata['data']);
		echo '_POST-> ';print_r($_POST);
		echo '_SESSION-> ';print_r($_SESSION);
		die();
		$pagedata['data'] = array(
			'check_in' => $this->check_in,
			'check_out' => $this->check_out,
			'adults' => $this->adults,
			'children' => $this->children,
			'total_nights' => $this->total_nights,
			'total_romms' => $this->total_romms,
			'gross_total' => $this->gross_total,
			'billData' => $this->billData,
			'paymentGatewayDetails' => $this->paymentGatewayDetails
		);
		//$this->load->view('invoice');

		// Load Booking model
		$this->load->model('BookingProcess_model','bprocess');
		//$this->bprocess->BookingProcess();
		echo '<pre>';
		//echo 'userdata-> '; print_r($this->session->userdata());
		//echo '_GET-> ';print_r($_GET);
		echo '_POST-> ';print_r($_POST);
		echo '_SESSION-> ';print_r($_SESSION);
		die();

	}

	public function review()
	{
		$this->load->view('success');
	}
	
	public function success()
	{
		$this->load->view('review');
	}
}
