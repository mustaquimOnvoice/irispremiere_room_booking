<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('common_model');

	}

	public function index()
	{
		$this->load->view('index');
	}

	// public function booking_system()
	// {
		// $this->form_validation->set_rules('check_in', 'check_in', 'required');
        // $this->form_validation->set_rules('check_out', 'check_out', 'required');
        // $this->form_validation->set_rules('room', 'room', 'required');
        // $this->form_validation->set_rules('adults', 'Adults', 'required');
        // $this->form_validation->set_rules('children', 'Children', 'required');
    	
		// if ($this->form_validation->run()) {
            //print_r($_POST);exit;
			
			/*$session_array=array(
				$check_in = $this->input->post('check_in'),
				$check_out = $this->input->post('check_out'),
				$adults = $this->input->post('adults'),
				$children = $this->input->post('children')
			);
			$this->session->set_userdata($session_array);*/
		// 	redirect(site_url('booking'));
        // }
		// $this->load->view('index');
	// 	$this->load->view('booking_system');
	// }

	// public function payment()
	// {
	// 	$this->load->view('payment');
	// }

	// public function review()
	// {
	// 	$this->load->view('review');
	// }

	// public function success()
	// {
	// 	$this->load->view('success');
	// }

	// public function failure()
	// {
	// 	$this->load->view('failure');
	// }

}
